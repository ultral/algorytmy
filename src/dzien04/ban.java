import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class ban {

  /*
    private static class PunktComparator implements Comparator<Punkt> {

      public int compare(Punkt p1, Punkt p2) {
        if (p1.p > p2.p) {
          return 1;
        }
        if (p2.p > p1.p) {
          return -1;
        }
        if (p1.k > p2.k) {
          return 1;
        }
        if (p2.k > p1.k) {
          return -1;
        }
        return 0;
      }
    }

    private static class PunktComparator2 implements Comparator<Punkt> {

      public int compare(Punkt p1, Punkt p2) {
        if (p1.k > p2.k) {
          return 1;
        }
        if (p2.k > p1.k) {
          return -1;
        }
        if (p1.p > p2.p) {
          return 1;
        }
        if (p2.p > p1.p) {
          return -1;
        }
        return 0;
      }
    }

    private static class Punkt{
      public int p, k;
      public Punkt(int p, int k){
        this.p = p ; this.k = k;
      }

      public String toString() {
        return p+" "+k;
      }
    }
    private static Punkt pun[];

    private static void wypiszPun(){
      System.out.printf("pun='%s'\n", Arrays.toString(pun));
    }

  */
  private static void wypisz() {
    System.out.printf("po='%s'\n", Arrays.toString(po));
    System.out.printf("ko='%s'\n", Arrays.toString(ko));
  }

  private static int n;
  private static int po[], ko[];
  private static int suma = 0;


  private static void readInput() {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    try {
      StringTokenizer st = new StringTokenizer(br.readLine());
      n = Integer.parseInt(st.nextToken());
      po = new int[n];
      ko = new int[n];
      for (int i = 0; i < n; i++) {
        st = new StringTokenizer(br.readLine());
        po[i] = Integer.parseInt(st.nextToken());
        ko[i] = Integer.parseInt(st.nextToken());
        if (po[i] == ko[i]) {
          i--;
          n--;
        }
      }
      //wypisz();
      Arrays.sort(po, 0, n);
      Arrays.sort(ko, 0, n);
      //wypisz();

    } catch (IOException e) {
      System.out.print("IOException: " + e.toString());
    }

  }

  private static void writeOutput() {
    System.out.print(suma);
  }

  private static int i1, i2, imin;
  private static int itmpmin = Integer.MAX_VALUE;

  private static void spr(int itmp, int imintmp) {
    if (i1 + i2 == 0) {
      i1 = itmp;
      imin = itmp + 1;
    } else {

      int w1 = i1 - imin + i2;
      int w2 = i1 - imintmp + itmp;
      int w3 = i1 - imin + itmp;
      int w4 = i2 - imintmp + itmp;

      if (w1>w2){
        if (w1>w3){
          if (w4>w1){
            i1 = i2; imin = imintmp; i2 = itmp;
            itmpmin = Integer.MAX_VALUE;
          }
        } else {
          if (w3 > w4){
            i2 = itmp;
          } else {
            i1 = i2; imin = imintmp; i2 = itmp;
            itmpmin = Integer.MAX_VALUE;
          }
        }
      } else {
        if (w2>w3) {
          if (w2>w4) {
            imin = imintmp; i2 = itmp;
            itmpmin = Integer.MAX_VALUE;
          } else {
            i1 = i2; imin = imintmp; i2 = itmp;
            itmpmin = Integer.MAX_VALUE;
          }
        } else {
          if (w3>w4){
            i2 = itmp;
          } else {
            i1 = i2; imin = imintmp; i2 = itmp;
            itmpmin = Integer.MAX_VALUE;
          }
        }
      }

    }
  }



  private static void doMagic() {
    int itmp = 0;
    int j = 0;
    i1 = 0;
    i2 = 0;

    for (int i = 0; i < n; i++) {
      if (po[i] < ko[j]) {
        itmp++;
      } else {
        spr(itmp, itmpmin);
        while (po[i] >= ko[j]) {
          j++;
          itmp--;
        }
        if (itmp<itmpmin){
          itmpmin = itmp;
        }
        i--;
      }
    }
    spr(itmp, itmpmin);
    suma = i1 + i2 - imin;
  }

  public static void main(String[] args) {
    readInput();
    doMagic();
    writeOutput();
  }
}
