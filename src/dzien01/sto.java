import java.io.*;
import java.util.StringTokenizer;

public class sto {

  private static int x, y, k, szer, wys, ilosc = 0;

  private static void readInput() {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    try {
      StringTokenizer st = new StringTokenizer(br.readLine());
      x = Integer.parseInt(st.nextToken());
      y = Integer.parseInt(st.nextToken());
      k = Integer.parseInt(st.nextToken());
    } catch (IOException e) {
      System.out.print("IOException: " + e.toString());
    }

  }

  private static void writeOutput() {
    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
    try {
      bw.write(ilosc);
      bw.flush();
    } catch (IOException e) {
      System.out.print("IOException: " + e.toString());
    }
  }

  private static void doMagic() {
    if (x>=2*k){
      szer = 2;
    } else if (x>=k) {
      szer = 1;
    } else {
      szer = 0;
    }

    if (y>=2*k){
      wys = 2;
    } else if (y>=k) {
      wys = 1;
    } else {
      wys = 0;
    }

    ilosc = (x/k)*wys;
    ilosc = ilosc + (y/k)*szer - wys*szer;
  }

  public static void main(String[] args) {
    readInput();
    doMagic();
    writeOutput();
  }
}
