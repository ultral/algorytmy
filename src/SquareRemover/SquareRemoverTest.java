package SquareRemover;

import java.util.*;

public class SquareRemoverTest {
  private static final int maxSeconds = 30;
  private static final int up = 0;
  private static final int right = 1;
  private static final int down = 2;
  private static final int left = 3;
  private static class TestCase {
    int colors, size;
    long seed;
    String[] board;
  }

  private static long nextNum(long seed){
    return (seed * 48271)%2147483647;
  }

  private static void fillTcBoard(String[] board, long seed, int colors){
    for (int i = 0; i < board.length; i++){
      board[i] = String.valueOf(seed % colors);
      seed = nextNum(seed);
    }
  }



  private static TestCase giveTestCase(int colors, int size) {
    TestCase testCase;
    testCase = new TestCase();
    testCase.colors = colors;
    testCase.size = size;
    testCase.seed = colors*size;
    testCase.board = new String[testCase.size*testCase.size];
    fillTcBoard(testCase.board, testCase.seed, testCase.colors);
    return testCase;
  }

  static int minSize = 8, maxSize = 16, minColors = 4, maxColors = 6;

  private static Collection<TestCase> giveTestCasesSize(int size) {
    Collection<TestCase> testCases = new LinkedList<TestCase>();
    for (int i = minColors; i<=maxColors; i++){
      testCases.add(giveTestCase(i, size));
    }
    return testCases;
  }

  private static Collection<TestCase> giveTestCasesColor(int color) {
    Collection<TestCase> testCases = new LinkedList<TestCase>();
    for (int i = minSize; i<=maxSize; i++){
      testCases.add(giveTestCase(color, i));
    }
    return testCases;
  }

  private static Collection<TestCase> giveTestCasesAllSize() {
    Collection<TestCase> testCases = new LinkedList<TestCase>();
    for (int i = minSize; i<=maxSize; i++){
      testCases.addAll(giveTestCasesSize(i));
    }
    return testCases;
  }

  private static Collection<TestCase> giveTestCasesAllColors() {
    Collection<TestCase> testCases = new LinkedList<TestCase>();
    for (int i = minColors; i<=maxColors; i++){
      testCases.addAll(giveTestCasesColor(i));
    }
    return testCases;
  }

  private static Collection<TestCase> giveTestCasesAll() {
    Collection<TestCase> testCases = giveTestCasesAllSize();
    testCases.addAll(giveTestCasesAllColors());
    return testCases;
  }


  private static Collection<TestCase> giveTestCasesSpecial() {
    Collection<TestCase> testCases = new LinkedList<TestCase>();
    TestCase testCase;
    long seed;

    testCase = new TestCase();
    testCase.colors = maxColors;
    testCase.seed = 98353342;
    seed = testCase.seed;
    testCase.size = maxSize;
    testCase.board = new String[testCase.size*testCase.size];
    fillTcBoard(testCase.board, seed, testCase.colors);
    //testCases.add(testCase);

    return testCases;
  }

  private static String replaceInString(String source, String newPart, int index){
    return source.substring(0,index)+newPart+source.substring(index,source.length());
  }

  private static int adjustGameScore(TestCase tc) {
    int score = 0;

    for (int i = 0, si = tc.size-1; i < si; i++){
      for (int j = 0, sj = tc.size-1; j < sj; j++){
        if (tc.board[tc.size*i+j].equals(tc.board[tc.size*i+j+1]) &&
            tc.board[tc.size*i+j].equals(tc.board[tc.size*(i+1)+j]) &&
            tc.board[tc.size*i+j].equals(tc.board[tc.size*(i+1)+j+1])){

            tc.board[tc.size*i+j] = String.valueOf(tc.seed%tc.colors);
            tc.seed=nextNum(tc.seed);
            tc.board[tc.size*i+j+1] = String.valueOf(tc.seed%tc.colors);
            tc.seed=nextNum(tc.seed);
            tc.board[tc.size*(i+1)+j] = String.valueOf(tc.seed%tc.colors);
            tc.seed=nextNum(tc.seed);
            tc.board[tc.size*(i+1)+j+1] = String.valueOf(tc.seed%tc.colors);
            tc.seed=nextNum(tc.seed);
            score++;
            i-=1;j-=2;if (i<0) {i=0;}if (j<0) {j=-1;}
          }
        }
      }

    return score;
  }

  private static boolean makeMove(TestCase tc, int i, int j, int direction){
    String help = tc.board[tc.size*i+j];
    int i2 = i, j2 = j;
    switch (direction){
      case up: i2--;break;
      case down: i2++;break;
      case left: j2--;break;
      case right: j2++;break;
      default: return false;
    }
    if (i<0 || i>=tc.size) return false;
    if (j<0 || j>=tc.size) return false;


    tc.board[tc.size*i+j] = tc.board[tc.size*i2+j2];
    tc.board[tc.size*i2+j2] = help;
    return true;
  }

  private static int calculatePoints(TestCase tc, int[] moves) {
    int score = adjustGameScore(tc);
    for (int i = 0, s = moves.length; i< s; i = i+3){
      if (!makeMove(tc, moves[i], moves[i+1], moves[i+2])){
        System.out.println("Bad move number: "+ i+ " current score is: "+score);
        System.out.println("i: "+ moves[i]+ ", j: "+moves[i+1]+", direction: "+moves[i+2]);
        return -1;
      }
      score += adjustGameScore(tc);
    }

    points += score;
    tests++;
    return score;
  }

  private static void printBoard(String[] board, int size){
    for (int i = 0; i < size; i++){
      System.out.println(Arrays.toString(Arrays.copyOfRange(board,i*size, size*(i+1))));
    }
  }

  private static void printTestCase(TestCase tc){
    System.out.println("Colors - "+tc.colors);
    System.out.println("Start seed - "+tc.seed);
    printBoard(tc.board, tc.size);
    System.out.println("");
  }

  private static String[] makeBoard(String[] inBoard, int size){
    String[] board = new String[size];
    for (int i = 0; i < size; i++){
      for (int j= 0; j < size; j++) {
        if (board[i] == null) {
          board[i] = inBoard[size*i+j];
        } else {
          board[i] += inBoard[size*i+j];
        }
      }
    }
    return board;
  }

  private static long points = 0;
  private static int tests = 0;
  private static double heuristic = 1.0;

  private static void test(TestCase tc) {
    SquareRemover sr = new SquareRemover();
    if (setHeuristic){
      sr.heuristicModifier = heuristic;
    }
    int[] moves;
    long start = System.currentTimeMillis();
    moves = sr.playIt(tc.colors, makeBoard(tc.board, tc.size), (int)tc.seed);
    double timeSpent = ((double) (System.currentTimeMillis() - start) / 1000);
    //printTestCase(tc);
    int points = calculatePoints(tc, moves);
    if (printOutput) {
      System.out.println("Moves generation took: " + timeSpent + " seconds and score was: " + points + ", size: " + tc.size + ", colors: " + tc.colors);
    }
    if (timeSpent > maxSeconds) {
      System.out.println("Moves generation took: "+ timeSpent+ "(>"+ maxSeconds +") seconds and it was too long!");
    }
    sr = null;
    System.gc();
  }

  private static boolean printOutput = false;
  private static boolean setHeuristic = true;

  public static void main(String[] args) {
    double[] heuristics = new double[] {0.25,0.375,0.5,0.675,0.75};
    for (double h : heuristics){
        points = 0; tests=0;
        runTest(h);
    }
  }

  private static void runTest(double inHeuristic) {
    Collection<TestCase> testCases = giveTestCasesSpecial();
    testCases.addAll(giveTestCasesColor(6));
    testCases.addAll(giveTestCasesColor(5));
    testCases.addAll(giveTestCasesColor(4));
    heuristic = inHeuristic;
    for (TestCase tc : testCases){
      test(tc);
    }
    System.out.print("Avg moves: "+ points/tests);
    if (setHeuristic)
      System.out.println(" heuristic modifier: "+heuristic);
    else {
      System.out.println(" default heuristic.");
    }

  }
}
