package SquareRemover;

import java.util.*;

/**
 * Konrad Błachnio for TopCoder Marathon Match SquareRemover Contest.
 */
public class SquareRemover {
  private static final int maxSeconds = 300000;
  private static final double fastExecution = 0.95;
  private static final int miliSeconds = 1000;
  private static final int up = 0;
  private static final int right = 1;
  private static final int down = 2;
  private static final int left = 3;
  private static final int squareSize = 4;
  private static final int calculatedColors = 4;
  private static final int movesNo = 10000;
  private static final int moveSize = 3;
  private static final int twoBonusCross = 2;
  private static final int twoBonus = 5;
  private static final int treeBonus = 10;
  private static final int squareBonus = 100;
  private static final int hashMultiplier = 48271;
  private static final int hashDivider = 2147483647;
  private static final int[] iMod = new int[] {-1, 0, 1, 0};
  private static final int[] jMod = new int[] {0, 1, 0, -1};
  private final int[] move = new int[moveSize];
  private final int[] moves = new int[movesNo*moveSize];
  private final LinkedList<Move> generatedMoves =  new LinkedList<Move>();
  private final LinkedList<Integer> nextColors =  new LinkedList<Integer>();
  private final Collection<Point> squarePoints = new HashSet<Point>(4);
  private final Collection<Point> tmpPoints = new HashSet<Point>(4);
  private int movesGenerated = 0;
  private int[][] board, tmpBoard;
  private int colors;
  private int size;
  private int gameScore;
  private int seed;
  private long timeStart;
  private Random rand;
  private Map<Integer, Collection<Point>> pointsColors;
  private Point[][] pointsTab;

  private void addMoveToList(final int i, final int j, final int moveType, int[][] tmpBoard, LinkedList<Move> moves){
    makeMove(i,j, moveType, tmpBoard);
    moves.addLast(new Move(i,j, moveType));
  }

/*  private void checkMove(){
    if (tmpBoard[lastSquareData.i][lastSquareData.j] != lastSquareData.color ||
        tmpBoard[lastSquareData.i+1][lastSquareData.j] != lastSquareData.color ||
        tmpBoard[lastSquareData.i+1][lastSquareData.j+1] != lastSquareData.color ||
        tmpBoard[lastSquareData.i][lastSquareData.j+1] != lastSquareData.color) {
      System.out.println("Board");
      printBoard(board);
      System.out.println("tmpBoard");
      printBoard(tmpBoard);
      System.out.println("BAD MOVES");
    }
  }*/

  private boolean isFinalDoubleMove(final int i, final int j, final int moveType, int[][] tmpBoard, LinkedList<Move> moves){
    if (getSwapColor(i, j, moveType, tmpBoard) ==  tmpBoard[i][j]) {
      //we must go to destination point through point of our color so we move that point instead and move ourself next
      addMoveToList(i + iMod[moveType], j + jMod[moveType], moveType, tmpBoard, moves);
      addMoveToList(i, j, moveType, tmpBoard, moves);
      //checkMove();
      return true;
    } else {
      return false;
    }
  }

  private int countLocalBoardTwo(final int minI, final int maxI, final int minJ, final int maxJ, final int[][] tmpBoard) {
    int value = 0;

    for (int i = minI; i<maxI; i++)
      for (int j = minJ; j<=maxJ; j++)
        if (tmpBoard[i][j] == tmpBoard[i+1][j]) {
          value += twoBonus;
        }

    for (int i = minI; i<=maxI; i++)
      for (int j = minJ; j<maxJ; j++)
        if (tmpBoard[i][j] == tmpBoard[i][j+1]) {
          value += twoBonus;
        }

    for (int i = minI; i<maxI; i++)
      for (int j = minJ; j<maxJ; j++)
        if (tmpBoard[i][j] == tmpBoard[i+1][j+1] ||
            tmpBoard[i+1][j] == tmpBoard[i][j+1]) {
          value += twoBonusCross;
        }

    return value;
  }

  private int countLocalBoardThree(final int minI, final int maxI, final int minJ, final int maxJ, final int[][] tmpBoard) {
    int value = 0;

    for (int i = minI; i<maxI; i++)
      for (int j = minJ; j<maxJ; j++)
        if ((tmpBoard[i][j] == tmpBoard[i][j+1] && tmpBoard[i][j] == tmpBoard[i+1][j]) ||
            (tmpBoard[i][j] == tmpBoard[i][j+1] && tmpBoard[i][j] == tmpBoard[i+1][j+1]) ||
            (tmpBoard[i][j] == tmpBoard[i+1][j] && tmpBoard[i][j] == tmpBoard[i+1][j+1]) ||
            (tmpBoard[i+1][j+1] == tmpBoard[i+1][j] && tmpBoard[i][j] == tmpBoard[i][j+1])) {
          value += treeBonus;
          i+=2; if (i>=maxI) {i = maxI-1;}
        }

    return value;
  }

  private int countLocalBoardSquares(final int minI, final int maxI, final int minJ, final int maxJ, final int[][] tmpBoard) {
    int value = 0;

    for (int i = minI; i<maxI; i++)
      for (int j = minJ; j<maxJ; j++)
        if (tmpBoard[i][j] == tmpBoard[i][j+1] &&
            tmpBoard[i][j] == tmpBoard[i+1][j] &&
            tmpBoard[i][j] == tmpBoard[i+1][j+1]) {
          value += squareBonus;
          i+=2; if (i>=maxI) {i = maxI-1;}
        }

    return value;
  }

  private int countLocalBoardValue(int minI, int maxI, int minJ, int maxJ, int[][] tmpBoard) {
    if (minI < 0) {minI = 0;}
    if (minJ < 0) {minJ = 0;}
    if (maxI > tmpBoard.length - 1) {maxI = tmpBoard.length - 1;}
    if (maxJ > tmpBoard.length - 1) {maxJ = tmpBoard.length - 1;}
    int value = countLocalBoardSquares(minI, maxI, minJ, maxJ, tmpBoard);
    value += countLocalBoardThree(minI, maxI, minJ, maxJ, tmpBoard);
    value += countLocalBoardTwo(minI, maxI, minJ, maxJ, tmpBoard);

    return value;
  }

  private void genMoveForPoints(final int i, final int j, Point destPoint, int[][] tmpBoard, LinkedList<Move> moves){
    int moveType;
    //move in one line (one direction)
    if (destPoint.i == i || destPoint.j == j){
      moveType = getMoveType(destPoint.i, i, destPoint.j, j);
      if (isFinalDoubleMove(i, j, moveType, tmpBoard, moves)) {return;}
    } else {
      //move in several directions
      int upDownMove = upDown(destPoint.i, i), leftRightMove = leftRight(destPoint.j, j);
      //here we choose where to move, so we count compare two moves
      if (getSwapColor(i, j, upDownMove, tmpBoard) != tmpBoard[i][j] && getSwapColor(i, j, leftRightMove, tmpBoard) != tmpBoard[i][j]){
        makeMove(i,j, upDownMove, tmpBoard);
        int upDownValue = countLocalBoardValue(Math.min(i,i+iMod[upDownMove])-1, Math.max(i,i+iMod[upDownMove])+1,
                                     Math.min(j,j+jMod[upDownMove])-1, Math.max(j,j+jMod[upDownMove])+1, tmpBoard);
        revertMove(i,j, upDownMove, tmpBoard);
        makeMove(i,j, leftRightMove, tmpBoard);
        int leftRightValue = countLocalBoardValue(Math.min(i,i+iMod[leftRightMove])-1, Math.max(i,i+iMod[leftRightMove])+1,
                                        Math.min(j,j+jMod[leftRightMove])-1, Math.max(j,j+jMod[leftRightMove])+1, tmpBoard);
        revertMove(i,j, leftRightMove, tmpBoard);

        if (upDownValue > leftRightValue) {
          moveType = upDownMove;
        } else {
          moveType = leftRightMove;
        }
      } else {
        //we can't compare move because one move is forbidden
        moveType = upDown(destPoint.i, i);
        if (getSwapColor(i, j, moveType, tmpBoard) ==  tmpBoard[i][j]) {
          moveType = leftRight(destPoint.j, j);
/*          if (getSwapColor(i, j, moveType, tmpBoard) ==  tmpBoard[i][j]){
            System.out.println("Board");
            printBoard(board);
            System.out.println("tmpBoard");
            printBoard(tmpBoard);
            System.out.println("genMoveForPoints error");
          }*/
        }
      }
    }

    addMoveToList(i,j,moveType,tmpBoard,moves);
    if (destPoint.i != i+iMod[moveType] || destPoint.j != j+jMod[moveType]) {
      //we are not at the destination point we must process further
      genMoveForPoints(i+iMod[moveType], j+jMod[moveType], destPoint, tmpBoard, moves);
    }
  }
  private int leftRight(final int destinationJ, final int sourceJ) {
    if (destinationJ > sourceJ) {return right;} else {return left;}
  }
  private int upDown(final int destinationI, final int sourceI) {
    if (destinationI > sourceI) {return down;} else {return up;}
  }
  private int getSwapColor(final int i, final int j, final int direction, final int[][] tmpBoard){
    return tmpBoard[i+iMod[direction]][j+jMod[direction]];
  }
  private int getMoveType(final int destI, final int sourceI, final int destJ, final int sourceJ){
    if (destI == sourceI) {
      return leftRight(destJ, sourceJ);
    } else {
      return upDown(destI, sourceI);
    }
  }

  private final Map<Point, Point> movesMap = new HashMap<Point, Point>(4);

  private int getBestMovesCombinationRec(Point[] squarePoints, Point[] tmpPoints){

    if (squarePoints.length == 1) return squarePoints[0].distanceTo(tmpPoints[0]);
    Point[] squarePointsRec = new Point[squarePoints.length-1], tmpPointsRec = new Point[tmpPoints.length-1];
    Point[] squarePointsRet = new Point[squarePoints.length], tmpPointsRet = new Point[tmpPoints.length];
    int minMoves = Integer.MAX_VALUE, currMoves;

    for (int i = 0; i < squarePoints.length; i++) {
      System.arraycopy(squarePoints, 0, squarePointsRec, 0, i);
      System.arraycopy(squarePoints, i+1, squarePointsRec, i, squarePoints.length-1-i);

      for (int j = 0; j < tmpPoints.length; j++) {
        System.arraycopy(tmpPoints, 0, tmpPointsRec, 0, j);
        System.arraycopy(tmpPoints, j+1, tmpPointsRec, j, tmpPoints.length-1-j);
        currMoves = squarePoints[i].distanceTo(tmpPoints[j]);
        currMoves+= getBestMovesCombinationRec(squarePointsRec, tmpPointsRec);
        if (currMoves<minMoves){
          minMoves = currMoves;
          squarePointsRet[0] = squarePoints[i];
          System.arraycopy(squarePointsRec, 0, squarePointsRet, 1, squarePointsRec.length);
          tmpPointsRet[0] = tmpPoints[j];
          System.arraycopy(tmpPointsRec, 0, tmpPointsRet, 1, tmpPointsRec.length);
        }
      }
    }
    if (tmpPointsRet[0]==tmpPointsRet[1]){
      System.out.println("Double");
    }
    System.arraycopy(squarePointsRet, 0, squarePoints, 0, squarePointsRet.length);
    System.arraycopy(tmpPointsRet, 0, tmpPoints, 0, squarePointsRet.length);
    if (tmpPoints[0]==tmpPoints[1]){
      System.out.println("Double");
    }
    return minMoves;
  }

  private void genBestMovesCombination(Point[] squarePoints, Point[] tmpPoints){
    if (squarePoints.length == 1) return;

    getBestMovesCombinationRec(squarePoints, tmpPoints);
/*    if (tmpPoints.length > 1 && tmpPoints[0]==tmpPoints[1]){
      System.out.println("Double");
    }*/
    Point tmpPoint;
    for (int i = 0; i < squarePoints.length; i++){
      for (int j = 0; j < squarePoints.length-1; j++){
        if (squarePoints[j].distanceTo(tmpPoints[j]) > squarePoints[j+1].distanceTo(tmpPoints[j+1])){
          tmpPoint=squarePoints[j];squarePoints[j]=squarePoints[j+1];squarePoints[j+1]=tmpPoint;
          tmpPoint=tmpPoints[j];tmpPoints[j]=tmpPoints[j+1];tmpPoints[j+1]=tmpPoint;
        }
      }
    }
/*    for (int i = 0; i < squarePoints.length-1; i++){
      if (squarePoints[i].distanceTo(tmpPoints[i]) > squarePoints[i+1].distanceTo(tmpPoints[i+1]))
        System.out.println("Moves not sorted.");
    }

    if (tmpPoints.length > 1 && tmpPoints[0]==tmpPoints[1]){
      System.out.println("Double");
    }*/
  }


  private LinkedList<Move> makeMovesFromSquareData(SquareFillData sfd, Point[][] inPointsTab){
    final int i = sfd.i, j = sfd.j;
    squarePoints.clear();
    tmpPoints.clear();
    if (sfd.leftTop != inPointsTab[i][j]) {squarePoints.add(sfd.leftTop); tmpPoints.add(inPointsTab[i][j]);}
    if (sfd.rightTop != inPointsTab[i][j+1]) {squarePoints.add(sfd.rightTop); tmpPoints.add(inPointsTab[i][j+1]);}
    if (sfd.leftBot != inPointsTab[i+1][j]) {squarePoints.add(sfd.leftBot); tmpPoints.add(inPointsTab[i+1][j]);}
    if (sfd.rightBot != inPointsTab[i+1][j+1]) {squarePoints.add(sfd.rightBot); tmpPoints.add(inPointsTab[i+1][j+1]);}
    generatedMoves.clear();

    Point[] squareArray = new Point[squarePoints.size()], tmpArray = new Point[tmpPoints.size()];
    genBestMovesCombination(squarePoints.toArray(squareArray), tmpPoints.toArray(tmpArray));
    for (int index = 0; index < squareArray.length; index++){
      genMoveForPoints(squareArray[index].i, squareArray[index].j, tmpArray[index], tmpBoard, generatedMoves);
    }

    //checkMove();

    return generatedMoves;
  }

  private int countSquareValue(SquareFillData sfd, int[][] inBoard) {
    if (sfd == null) return Integer.MIN_VALUE;
    int[] tmp = new int[4];
    tmp[0] = inBoard[sfd.i][sfd.j]; inBoard[sfd.i][sfd.j] = nextColors.get(0);
    tmp[1] = inBoard[sfd.i][sfd.j+1]; inBoard[sfd.i][sfd.j+1] = nextColors.get(1);
    tmp[2] = inBoard[sfd.i+1][sfd.j]; inBoard[sfd.i+1][sfd.j] = nextColors.get(2);
    tmp[3] = inBoard[sfd.i+1][sfd.j+1]; inBoard[sfd.i+1][sfd.j+1] = nextColors.get(3);
    int value = countLocalBoardValue(sfd.i-1,sfd.i+2,sfd.j-1,sfd.j+2, inBoard);
    inBoard[sfd.i][sfd.j] = tmp[0];
    inBoard[sfd.i][sfd.j+1] = tmp[1];
    inBoard[sfd.i+1][sfd.j] = tmp[2];
    inBoard[sfd.i+1][sfd.j+1] = tmp[3];
    return value;
  }

  private SquareFillData selectBestSquare(Collection<SquareFillData> minSquareSet, int[][] inBoard){
    if (minSquareSet.isEmpty()) return null;
    SquareFillData bestSquare = minSquareSet.iterator().next();
    if (minSquareSet.size() == 1) return bestSquare;
    int max = Integer.MIN_VALUE, curr;

    for (SquareFillData sqf : minSquareSet){
      curr = countSquareValue(sqf, inBoard);
      if (max < curr) {
        max = curr; bestSquare = sqf;
      }
    }

    return bestSquare;
  }

  private final Map<Integer, Collection<SquareFillData>> boardSquareFillData = new HashMap<Integer, Collection<SquareFillData>>();
  private final int worseMoveModifier = 1;
  public double heuristicModifier = 1;
  //public SquareFillData lastSquareData;
  private LinkedList<Move> genMoves(){
    int minValue = Integer.MAX_VALUE-worseMoveModifier;
    Collection<SquareFillData> minSquareSet = new HashSet<SquareFillData>(16);
    SquareFillData currSquareData, bestSquareData;
    Collection<Point> pointsCurr = new HashSet<Point>(4);

    for (Collection<SquareFillData> col : boardSquareFillData.values()){
      col.clear();
    }

    for (int i = 0; i< size-1; i++){
      for (int j = 0; j< size-1; j++){
        for (int col = 0; col< colors; col++){
          currSquareData = findMinMove(i, j, col, pointsCurr, squarePoints, pointsTab, pointsColors.get(col), minValue+worseMoveModifier);
          if (boardSquareFillData.get(currSquareData.value) == null) {
            boardSquareFillData.put(currSquareData.value, new HashSet<SquareFillData>());
          }
          boardSquareFillData.get(currSquareData.value).add(currSquareData);
          if (currSquareData.value<minValue) {
            minValue = currSquareData.value;
            minSquareSet.clear();minSquareSet.add(currSquareData);
          } else if (currSquareData.value == minValue){
            minSquareSet.add(currSquareData);
          }
        }
      }
    }
    currSquareData = selectBestSquare(boardSquareFillData.get(minValue+worseMoveModifier), tmpBoard);
    bestSquareData = selectBestSquare(boardSquareFillData.get(minValue), tmpBoard);
    int bestVal = countSquareValue(bestSquareData,tmpBoard), currVal = countSquareValue(currSquareData, tmpBoard);
    //TODO here optimalization
    //lastSquareData = bestSquareData;
    if (bestVal*heuristicModifier < currVal){
      bestSquareData = currSquareData;
      //lastSquareData = bestSquareData;
    }
    return makeMovesFromSquareData(bestSquareData, pointsTab);
  }

  private class SquareFillData {
    int i,j,color,value = Integer.MAX_VALUE-1;
    Point leftTop, rightTop, leftBot, rightBot;
    public boolean insertPoint(Point p){
      return insertPoint(p.i, p.j, p);
    }
    public boolean insertPoint(int i, int j, Point p){
      if (p.color == color){
        if (i == this.i && j == this.j){leftTop = p; return true;}
        if (i == this.i && j == this.j+1){rightTop = p; return true;}
        if (i == this.i+1 && j == this.j){leftBot = p; return true;}
        if (i == this.i+1 && j == this.j+1){rightBot = p; return true;}
      }
      return false;
    }
  }
  private SquareFillData findMinMove(final int i, final int j, final int col, final Collection<Point> points,
                                     final Collection<Point> inSquarePoints, final Point[][] inPointsTab,
                                     final Collection<Point> inPointsColors, final int currentMin) {
    SquareFillData squareData = new SquareFillData();
    if (inPointsColors.size()<squareSize) return squareData;
    squareData.i = i; squareData.j = j; squareData.color = col;
    int moveValue = 0, minVal, currValue;
    points.clear();inSquarePoints.clear();
    inSquarePoints.add(inPointsTab[i][j]);
    inSquarePoints.add(inPointsTab[i + 1][j]);
    inSquarePoints.add(inPointsTab[i][j + 1]);
    inSquarePoints.add(inPointsTab[i + 1][j + 1]);

    //we are finding points which are on their places at the beginning
    for (Point p : inSquarePoints){
      if (inPointsTab[p.i][p.j].color == col) {
        points.add(p); inPointsColors.remove(p);
        if (!squareData.insertPoint(p)){
          System.out.println("squareData.insertPoint1");
        }
      }
    }
    //if we found points on their places we don't need to look for better ones
    inSquarePoints.removeAll(points);

    Point currPoint = inSquarePoints.iterator().next();
    for (Point sp : inSquarePoints){
      minVal = Integer.MAX_VALUE;
      for (Point p : inPointsColors){
        currValue = p.distanceTo(sp);
        if (currValue<minVal){minVal = currValue; currPoint = p;}
      }
      if (!squareData.insertPoint(sp.i,sp.j,currPoint)){
        System.out.println("squareData.insertPoint");
      }
      points.add(currPoint);
      inPointsColors.remove(currPoint);
      moveValue+=minVal;
      if (moveValue>currentMin) {moveValue = Integer.MAX_VALUE-1; break;}
    }

    inPointsColors.addAll(points);
    squareData.value = moveValue;
/*    if (moveValue < Integer.MAX_VALUE-1 &&
        (squareData.leftBot.color != squareData.leftTop.color ||
        squareData.leftBot.color != squareData.rightTop.color ||
        squareData.leftBot.color != squareData.rightBot.color)) {
      System.out.println("squareData.not the same color");
    }*/
    return squareData;
  }
  private void printBoard(int[][] inBoard){
    System.out.print("   ");
    for (int i = 0; i<inBoard.length; i++)
      System.out.print(i%10+"  ");
    System.out.println();
    int i = 0;
    for (int[] inBoardPart : inBoard) {
      System.out.print(i++%10+" ");
      System.out.println(Arrays.toString(inBoardPart));
    }
  }
  private static long nextNum(long seed){
    return (seed * hashMultiplier)%hashDivider;
  }
  private int[] genRandomMove(){
    move[0] = rand.nextInt(size);
    if (move[0] == 0) move[0]++;
    if (move[0] == size-1) move[0]--;
    move[1] = rand.nextInt(size);
    if (move[1] == 0) move[1]++;
    if (move[1] == size-1) move[1]--;
    move[2] = rand.nextInt(4);
    return move;
  }
  private void init(int colors, String[] board, int startSeed){
    this.timeStart = System.currentTimeMillis();
    this.colors = colors;
    this.seed = startSeed;
    this.rand = new Random(startSeed);
    this.size = board.length;
    this.board = new int[size][size];
    this.tmpBoard = new int[size][size];
    this.pointsTab = new Point[size][size];


    //TODO
    if (this.colors == 4){
      this.heuristicModifier = 0.5;//3.125;
    } else if (this.colors == 5){
      this.heuristicModifier = 0.5;//3;
    } else if (this.colors == 6){
      this.heuristicModifier = 0.5;//3;
    }

    for (int i = 0; i< size; i++){
      for (int j = 0; j< size; j++){
        this.board[i][j] = Integer.valueOf(board[i].substring(j,j+1));
        this.pointsTab[i][j] = new Point(i,j,this.board[i][j]);
      }
    }
    pointsColors = new HashMap<Integer, Collection<Point>>(colors);
    for (int i = 0; i< colors; i++){
      pointsColors.put(i, new HashSet<Point>(size * size));
    }
    for (int i = 0; i< size; i++){
      for (int j = 0; j< size; j++){
        pointsColors.get(this.board[i][j]).add(pointsTab[i][j]);
      }
    }
    copyBoard(tmpBoard);
    for (int i = 0; i < calculatedColors; i++){
      nextColors.addLast(seed%colors);
      seed=(int)nextNum(seed);
    }

    adjustGameScore(this.board, this.tmpBoard);
  }
  private int nextColor(){
    nextColors.addLast(seed%colors);
    seed=(int)nextNum(seed);
    return nextColors.removeFirst();
  }
  private void makeMove(final int i, final int j, final int direction, final int[][] inBoard){
    int temp = inBoard[i][j];
    inBoard[i][j] =  inBoard[i+iMod[direction]][j+jMod[direction]];
    inBoard[i+iMod[direction]][j+jMod[direction]] = temp;
  }
  private void revertMove(final Move p, final int[][] inBoard){
    makeMove(p.i,p.j,p.direction,inBoard);
  }
  private void revertMove(final int i, final int j, final int direction, final int[][] inBoard){
    makeMove(i,j,direction,inBoard);
  }
  private void makeMove(int i, int j, int direction, int[][] inBoard, Point[][] inPointsTab){
    //swap colors on tables
    makeMove(i, j, direction, inBoard);
    //swap points
    Point p1 = inPointsTab[i][j];
    Point p2 = inPointsTab[i+iMod[direction]][j+jMod[direction]];
    inPointsTab[p1.i][p1.j]=p2;
    inPointsTab[p2.i][p2.j]=p1;
    //swap points i and j
    int temp;
    temp = p1.i;p1.i=p2.i;p2.i=temp;
    temp = p1.j;p1.j=p2.j;p2.j=temp;
  }
  private void makeMove(final Move move, final int[][] inBoard, final Point[][] inPointsTab){
    makeMove(move.i,move.j,move.direction,inBoard,inPointsTab);
  }
  private void changePointColor(int i, int j, int oldColor, int newColor){
    Point p = pointsTab[i][j];
    p.color = newColor;
    pointsColors.get(oldColor).remove(p);
    pointsColors.get(newColor).add(p);
  }
  private boolean adjustGameScore(int[][] inBoard, int[][] tmpBoard) {
    int score = 0;
    for (int i = 0, si = size-1; i < si; i++){
      for (int j = 0, sj = size-1; j < sj; j++){
        if (inBoard[i][j] == inBoard[i+1][j] && inBoard[i][j] == inBoard[i][j+1] && inBoard[i][j] == inBoard[i+1][j+1]){
          changePointColor(i,j,inBoard[i][j], nextColors.peekFirst()); inBoard[i][j] = nextColor();
          changePointColor(i,j+1,inBoard[i][j+1], nextColors.peekFirst()); inBoard[i][j+1] = nextColor();
          changePointColor(i+1,j,inBoard[i+1][j], nextColors.peekFirst()); inBoard[i+1][j] = nextColor();
          changePointColor(i+1,j+1,inBoard[i+1][j+1], nextColors.peekFirst()); inBoard[i+1][j+1] = nextColor();

          tmpBoard[i][j] = inBoard[i][j];
          tmpBoard[i][j+1] = inBoard[i][j+1];
          tmpBoard[i+1][j] = inBoard[i+1][j];
          tmpBoard[i+1][j+1] = inBoard[i+1][j+1];
          score++;
          i-=1;j-=2;if (i<0) {i=0;}if (j<0) {j=-1;}
        }
      }
    }

    gameScore += score;
    return score>0;
  }
  private boolean testGameScore(int[][] inBoard) {
    for (int i = 0, si = size-1; i < si; i++){
      for (int j = 0, sj = size-1; j < sj; j++){
        if (inBoard[i][j] == inBoard[i+1][j] && inBoard[i][j] == inBoard[i][j+1] && inBoard[i][j] == inBoard[i+1][j+1]){
          return true;
        }
      }
    }
    return false;
  }
  private int[] genTabMovesFromMoves(Collection<Move> moves){
    int[] tabMoves = new int[moves.size()*moveSize];
    int i = 0;
    for (Move m : moves) {
      tabMoves[i++] = m.i;
      tabMoves[i++] = m.j;
      tabMoves[i++] = m.direction;
    }
    return tabMoves;
  }
  private int randomMove = 0;
  private final LinkedList<Move> actualMoves = new LinkedList<Move>();
  private int[] genCurrentMove() {
    LinkedList<Move> currentMoves;
    actualMoves.clear();
    long now = System.currentTimeMillis();
    boolean fast = ((double) (now - timeStart) / miliSeconds ) > maxSeconds*fastExecution;
    if (fast) {
      randomMove++;
      return genRandomMove();
    } else {
      currentMoves = genMoves();
    }
    if (currentMoves.size() + movesGenerated > movesNo){
      for (int i = 0, s = currentMoves.size() + movesGenerated -movesNo; i< s; i++){
        currentMoves.removeLast();
      }
    }

    int currMoveNo = 1;
    boolean scoreUp;
    for (Move m : currentMoves) {
      makeMove(m, board, pointsTab);
      actualMoves.addLast(m);
/*      if (testGameScore(board) && currMoveNo != currentMoves.size()){
        System.out.println("Board");
        printBoard(board);
        System.out.println("tmpBoard");
        printBoard(tmpBoard);
        System.out.println("Error in generated moves earlier move is making point.");
      }*/
      scoreUp = adjustGameScore(board, tmpBoard);
      if (scoreUp && currMoveNo != currentMoves.size()) {
        copyBoard(tmpBoard);
        //testBoards();
        return genTabMovesFromMoves(actualMoves);
      } else if (!scoreUp && currMoveNo == currentMoves.size()) {
/*      System.out.println("tmpBoard");
        printBoard(tmpBoard);
        for (int i = actualMoves.size()-1;i>=0;i--){
          revertMove(actualMoves.get(i),tmpBoard);
        }
        System.out.println("revertTmpBoard");
        printBoard(tmpBoard);*/
        //System.out.println("Error in generated moves last move and did not make a point.");
        copyBoard(tmpBoard);
      }
      currMoveNo++;
    }

    //testBoards();

    return genTabMovesFromMoves(actualMoves);
  }
  private void testBoards(){
    for (int i = 0; i< size; i++){
      for (int j = 0; j< size; j++) {
        if (board[i][j] != tmpBoard[i][j] ||
            board[i][j] != pointsTab[i][j].color ||
            pointsTab[i][j].i != i ||
            pointsTab[i][j].j != j){
          System.out.println("board test failed");
        }
      }
    }
  }
  private void copyBoard(int[][] destBoard){
    for (int i = 0; i< size; i++){
      System.arraycopy(this.board[i], 0, destBoard[i], 0, size);
    }
  }
  private int[] genatareMoves(){
    int[] currentMove;
    while (movesGenerated < movesNo){
      currentMove = genCurrentMove();
      System.arraycopy(currentMove, 0, moves, movesGenerated * moveSize, currentMove.length);
      movesGenerated += currentMove.length/moveSize;
    }
    return moves;
  }
  private void testOfDataStructures(){
    Point p;
    for (int i = 0; i< size; i++){
      for (int j = 0; j< size; j++){
        p = pointsTab[i][j];
        if (p.i != i || p.j != j || p.color != board[i][j]){
          System.out.println("Final test: FAILURE");
        }
      }
    }
  }
  public int[] playIt(int colors, String[] board, int startSeed){
    init(colors, board, startSeed);
    int[] outputMoves = genatareMoves();
    //testOfDataStructures();
    if (randomMove>0) {
      System.out.println("Random Moves no: " + randomMove);
    }
    //System.out.println("Final score: "+ gameScore);
    return outputMoves;
  }
  private class Move {
    int i,j,direction;
/*    Move(int[] tabMove){
      i = tabMove[0];j = tabMove[1]; direction = tabMove[2];
    }*/
    Move(int i, int j, int direction){
      this.i = i; this.j = j; this.direction = direction;
    }
  }
  private class Point {
    int i,j,color;
    public int distanceTo(Point p){
      return Math.abs(i-p.i)+Math.abs(j-p.j);
    }
/*    public int distanceTo(int i, int j){
      return Math.abs(this.i-i)+Math.abs(this.j-j);
    }*/
    Point(int i, int j, int color){
      this.i = i; this.j = j; this.color = color;
    }
/*    Point(int i, int j){
      this.i = i; this.j = j;
    }*/
  }
}
