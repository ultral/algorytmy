package pa2015.dzien2.mis;

import java.io.*;
import java.util.*;

public class mis {

    private static Integer[] vertex;
    private static HashMap<Integer, Set<Integer>> graph;
    private static Set<Integer> ok;
    private static LinkedList<Integer> toDelete;
    private static int n, m, d;

    private static void delete(Integer vert) {
        for (int i : graph.get(vert)) {
            graph.get(i).remove(vert);
            if (graph.get(i).size() < d) {
                toDelete.add(vertex[i]);
                ok.remove(vertex[i]);
            }
        }
    }

    private static void doMagic() {
        for (int i = 1; i <=n; i++) {
            if (graph.get(i).size() < d) {
                toDelete.add(vertex[i]);
            } else {
                ok.add(vertex[i]);
            }
        }

        while (!toDelete.isEmpty()) {
            delete(toDelete.removeFirst());
        }


        if (ok.isEmpty()) {
            System.out.println("NIE");
            return;
        }

        Set<Integer> biggestSet = Collections.emptySet(), currentSet;

        while (!ok.isEmpty()) {
            Integer curr = ok.iterator().next();
            ok.remove(curr);
            currentSet = getSubset(curr, new HashSet<Integer>(){{add(curr);}});
            if (currentSet.size() > biggestSet.size()) {
                biggestSet = currentSet;
            }
        }

        System.out.println(biggestSet.size());
        Integer[] arr = new Integer[biggestSet.size()];
        biggestSet.toArray(arr);
        Arrays.sort(arr);
        StringBuilder sb = new StringBuilder();
        for (Integer i : arr) {
            sb.append(i).append(" ");
        }
        System.out.print(sb.toString());
    }

    private static HashSet<Integer> getSubset(Integer vertex, HashSet<Integer> hs){
        if (graph.get(vertex).isEmpty())
            return hs;

        hs.addAll(graph.get(vertex));
        Set<Integer> tmp = graph.get(vertex);
        ok.removeAll(tmp);
        graph.put(vertex, Collections.emptySet());
        for (Integer i : tmp) {
            graph.get(i).remove(vertex);
        }

        for (Integer i : tmp) {
            getSubset(i, hs);
        }

        return hs;
    }


    private static void readInput(String[] args) {
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(args[0]));
        } catch (FileNotFoundException | IndexOutOfBoundsException e) {
            br = new BufferedReader(new InputStreamReader(System.in));
        }
        try {
            StringTokenizer st = new StringTokenizer(br.readLine());
            n = Integer.parseInt(st.nextToken());
            m = Integer.parseInt(st.nextToken());
            d = Integer.parseInt(st.nextToken());

            vertex = new Integer[n+1];
            graph = new HashMap<>(n);
            toDelete = new LinkedList<>();
            ok = new HashSet<>(n);
            for (int i = 1; i <= n; i++) {
                vertex[i] = i;
                graph.put(vertex[i], new HashSet<>(n/m));
            }

            int first, second;
            for (int i = 0; i < m; i++) {
                st = new StringTokenizer(br.readLine());
                first = Integer.parseInt(st.nextToken());
                second = Integer.parseInt(st.nextToken());
                graph.get(first).add(vertex[second]);
                graph.get(second).add(vertex[first]);
            }

        } catch (IOException ignore) {}

    }

    public static void main(String[] args) {
        readInput(args);
        doMagic();
    }
}
