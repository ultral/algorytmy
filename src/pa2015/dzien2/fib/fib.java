package pa2015.dzien2.fib;

import java.io.*;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.StringTokenizer;

public class fib {

    private static final BigInteger max = new BigInteger("10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000");
    private static BigInteger iterator = BigInteger.ONE;
    private static String f0 = "0";
    private static char[] f0c = new char[18];
    private static String f1 = "1";
    private static char[] f1c = new char[18];
    private static char[] sum = new char[18];

    static {
        Arrays.fill(f0c, (char)0);
        f0c[0] = '0';
        Arrays.fill(f1c, (char)0);
        f1c[0] = '1';
    }

    private static void sum(){
        int add = 0;
        for (int i = 0; i < 18; i++) {
            if (f0c[i] != 0 && f1c[i] != 0) {
                sum[i] = (char) (f0c[i] + f1c[i] - '0' + add);
            } else if (f0c[i] != 0 || f1c[i] != 0) {
                sum[i] = (char) (f0c[i] + f1c[i] + add);
            } else if (add > 0) {
                sum[i] = '1';
            }
            if (sum[i] - '0' > 9) {
                sum[i] = (char) (sum[i] - 10);
                add = 1;
            } else {
                add = 0;
            }
        }
    }

    private static String nextFib(){
        iterator = iterator.add(BigInteger.ONE);
        String f2 = new BigInteger(f0).add(new BigInteger(f1)).toString();
        int len = f2.length();
        f2 = f2.substring(len > 18? len - 18 : 0 ,len);
        f0 = f1;
        f1 = f2;

        System.out.println(f2);
        return f2;
    }

    private static void nextFibc(){
        iterator = iterator.add(BigInteger.ONE);
        sum();
        for (int i = 0; i < 18; i++) {
            f0c[i] = f1c[i];
            f1c[i] = sum[i];
        }
    }

    private static boolean endsWith(){

        for (int i = 0, max = endc.length; i < max; i++) {
            if (endc[max-i-1] != f1c[i]) {
                return false;
            }
        }

        return true;
    }


    private static void doMagic() {
        if ("0".endsWith(end)) {
            System.out.println("0");
            return;
        }
        if ("1".endsWith(end)) {
            System.out.println("1");
            return;
        }
/*      //to slow
        while (iterator.compareTo(max) == -1) {
            if (nextFib().endsWith(end)) {
                System.out.println(iterator);
                return;
            }
        }*/

        iterator = BigInteger.ONE;
        while (iterator.compareTo(max) == -1) {
            nextFibc();
            if (endsWith()) {
                System.out.println(iterator);
                return;
            }
        }


        System.out.println("NIE");
    }

    private static String end;
    private static char[] endc;

    private static void readInput(String[] args) {
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(args[0]));
        } catch (FileNotFoundException | IndexOutOfBoundsException e) {
            br = new BufferedReader(new InputStreamReader(System.in));
        }
        try {
            StringTokenizer st = new StringTokenizer(br.readLine());
            end = st.nextToken();
            endc = end.toCharArray();
        } catch (IOException ignore) {}

    }

    public static void main(String[] args) {
        readInput(args);
        doMagic();
    }
}
