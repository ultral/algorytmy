package pa2015.dzien1.kie;

import java.io.*;
import java.util.StringTokenizer;

public class kie {
    private static int sum = 0;

    private static void readInputAndDoMagic(BufferedReader br) {
        int current, n, minOdd = Integer.MAX_VALUE;
        try {
            StringTokenizer st = new StringTokenizer(br.readLine());
            n = Integer.parseInt(st.nextToken());
            st = new StringTokenizer(br.readLine());

            for (int i = 0; i < n; i++) {
                current = Integer.parseInt(st.nextToken());
                if (current % 2 == 0) {
                    sum += current;
                } else {
                    sum += current;
                    if (current < minOdd) {
                        minOdd = current;
                    }
                }
            }

            if (sum % 2 != 0) {
                sum -= minOdd;
            }

        } catch (IOException e) {
            System.out.print("IOException: " + e.toString());
        }

    }

    private static void writeOutput() {
        if (sum == 0) {
            System.out.println("NIESTETY");
        } else {
            System.out.println(sum);
        }

    }

    public static void main(String[] args) {
        BufferedReader br;
        if (args.length == 0) {
            br = new BufferedReader(new InputStreamReader(System.in));
        } else {
            try {
                br = new BufferedReader(new FileReader(args[0]));
            } catch (FileNotFoundException e) {
                br = new BufferedReader(new InputStreamReader(System.in));
            }
        }
        readInputAndDoMagic(br);
        writeOutput();
    }
}
