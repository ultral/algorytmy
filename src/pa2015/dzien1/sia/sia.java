package pa2015.dzien1.sia;

import java.io.*;
import java.math.BigInteger;
import java.util.StringTokenizer;

public class sia {
    private static int n, m;
    private static int[] rosnie;
    private static long[] aktualne;

    private static void doMagic(String[] args) {
        BufferedReader br;
        if (args.length == 0) {
            br = new BufferedReader(new InputStreamReader(System.in));
        } else {
            try {
                br = new BufferedReader(new FileReader(args[0]));
            } catch (FileNotFoundException e) {
                br = new BufferedReader(new InputStreamReader(System.in));
            }
        }

        try {
            StringTokenizer st = new StringTokenizer(br.readLine());
            n = Integer.parseInt(st.nextToken());
            m = Integer.parseInt(st.nextToken());
            st = new StringTokenizer(br.readLine());
            aktualne = new long[n];
            rosnie = new int[n];
            for (int i = 0; i < n; i++) {
                rosnie[i] = Integer.parseInt(st.nextToken());
            }

            long dzien, przyciecie, poprzednidzien = 0;
            for (int i = 0; i < m; i++) {
                st = new StringTokenizer(br.readLine());
                dzien = Long.parseLong(st.nextToken());
                przyciecie = Long.parseLong(st.nextToken());
                przytnij(dzien, przyciecie, poprzednidzien);
                poprzednidzien = dzien;
            }


        } catch (IOException e) {
            System.out.print("IOException: " + e.toString());
        }

    }

    private static void przytnij(long dzien, long przyciecie, long poprzednidzien) {
        BigInteger sum = BigInteger.ZERO;
        long roznica = dzien - poprzednidzien;
        for (int i = 0; i < n; i++) {
            aktualne[i] += rosnie[i]*(roznica);
            if (aktualne[i] > przyciecie) {
                sum = sum.add(BigInteger.valueOf(aktualne[i] - przyciecie));
                aktualne[i] = przyciecie;
            }
        }

        System.out.println(sum.toString());
    }

    public static void main(String[] args) {
        doMagic(args);
    }
}
