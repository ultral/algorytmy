package dijkstra;

import java.util.*;

public class Dijkstra {

    private static Random r = new Random(System.currentTimeMillis());

    private static void addEdge(int[][] graph, int i, int j, int dist) {
        dist = dist < 1 ? 1 : dist;
        graph[i][j] = dist;
        graph[j][i] = dist;
    }

    private static void addEdge(ArrayList<LinkedList<Vertex>> graph, int i, int j, int dist) {
        dist = dist < 1 ? 1 : dist;
        graph.get(i).add(new Vertex(j, dist));
        graph.get(j).add(new Vertex(i, dist));
    }

    private static int[][] genRandomTableGraph(int graphSize, boolean dense) {
        int maxEdges, minEdges;
        int[][] tableGraph = new int[graphSize][graphSize];

        if (dense) {
            maxEdges = graphSize * graphSize / 2 - graphSize;
            minEdges = (int) (graphSize * graphSize / Math.log(graphSize));
        } else {
            maxEdges = (int) (graphSize * graphSize / (Math.log(graphSize) + 1)) - 1;
            minEdges = graphSize - 1;
        }

        int edges = r.nextInt(maxEdges) + minEdges;
        edges = edges > maxEdges ? maxEdges : edges;

        int randomVertex;
        for (int i = 1; i < graphSize; i++) {
            randomVertex = r.nextInt(i);
            addEdge(tableGraph, randomVertex, i, r.nextInt(graphSize) + 1);
            edges--;
        }

        while (edges-- > 0) {
            for (int i = r.nextInt(graphSize); i < graphSize; i++) {
                for (int j = r.nextInt(graphSize); j < graphSize; j++) {
                    if (i != j && tableGraph[i][j] == 0) {
                        addEdge(tableGraph, i, j, r.nextInt(graphSize) + 1);
                        i = graphSize;
                        j = graphSize;
                    }
                }
            }
        }
        return tableGraph;
    }


    private static Vertex[][] genRandomListGraph(int graphSize, boolean dense) {
        int maxEdges, minEdges;
        ArrayList<LinkedList<Vertex>> graph = new ArrayList<>(graphSize);

        if (dense) {
            maxEdges = graphSize * graphSize / 2 - graphSize;
            minEdges = (int) (graphSize * graphSize / Math.log(graphSize));
        } else {
            maxEdges = (int) (graphSize * graphSize / (Math.log(graphSize) + 1)) - 1;
            minEdges = graphSize - 1;
        }

        int edges = r.nextInt(maxEdges) + minEdges;
        edges = edges > maxEdges ? maxEdges : edges;

        int randomVertex;
        for (int i = 0; i < graphSize; i++) {
            graph.add(i, new LinkedList<>());
        }

        for (int i = 1; i < graphSize; i++) {
            randomVertex = r.nextInt(i);
            addEdge(graph, randomVertex, i, r.nextInt(graphSize) + 1);
            edges--;
        }

        for (int i = 0; i < graphSize; i++) {
            for (int j = i + 1; j < graphSize; j++) {
                if (i != j && r.nextBoolean()) {
                    addEdge(graph, i, j, r.nextInt(graphSize) + 1);
                    if (--edges == 0) {
                        i = graphSize; j = graphSize;
                    }
                }
            }
        }

        Vertex[][] listGraph = new Vertex[graphSize][];
        for (int i = 0; i < graphSize; i++) {
            listGraph[i] = new Vertex[graph.get(i).size()];
            int k = 0;
            for (Vertex vertex : graph.get(i)) {
                listGraph[i][k++] = vertex;
            }
        }

        return listGraph;
    }


    private static Vertex[][] genListGraph(int[][] tableGraph) {
        Vertex[][] listGraph = new Vertex[tableGraph.length][];

        int nonZero;
        for (int i = 0, max = tableGraph.length; i < max; i++) {
            nonZero = 0;
            for (int j = 0; j < max; j++) {
                nonZero += tableGraph[i][j] > 0 ? 1 : 0;
            }
            listGraph[i] = new Vertex[nonZero];
            for (int j = 0, k = 0; j < max; j++) {
                if (tableGraph[i][j] > 0) {
                    listGraph[i][k++] = new Vertex(j, tableGraph[i][j]);
                }
            }
        }

        return listGraph;
    }

    private static Vertex[][] genListGraph() {


        Vertex[][] listGraph = new Vertex[5][];

        listGraph[0] = new Vertex[]{new Vertex(1, 3), new Vertex(2, 15)};
        listGraph[1] = new Vertex[]{new Vertex(0, 3), new Vertex(2, 2), new Vertex(3, 16)};
        listGraph[2] = new Vertex[]{new Vertex(1, 2), new Vertex(3, 1), new Vertex(0, 15), new Vertex(4, 17)};
        listGraph[3] = new Vertex[]{new Vertex(2, 1), new Vertex(4, 5), new Vertex(1, 16)};
        listGraph[4] = new Vertex[]{new Vertex(3, 5), new Vertex(2, 17)};

        return listGraph;
    }

    private static int[][] genTableGraph() {
        int[][] tableGraph = new int[5][5];

        tableGraph[0][1] = 3;
        tableGraph[1][0] = 3;
        tableGraph[1][2] = 2;
        tableGraph[2][1] = 2;
        tableGraph[2][3] = 1;
        tableGraph[3][2] = 1;
        tableGraph[3][4] = 5;
        tableGraph[4][3] = 5;
        tableGraph[0][2] = 15;
        tableGraph[2][0] = 15;
        tableGraph[1][3] = 16;
        tableGraph[3][1] = 16;
        tableGraph[2][4] = 17;
        tableGraph[4][2] = 17;

        return tableGraph;
    }

    private static void writeOutput(int[] distance, int[] predecessor, int source, int dest, String info) {
        System.out.println(info + "Distance between Vertex: " + source + " and Vertex: " + dest + " is: " + distance[dest]);
    }

    private static void writeOutput(Vertex[] distance, int[] predecessor, int source, int dest, String info) {
        System.out.println(info + "Distance between Vertex: " + source + " and Vertex: " + dest + " is: " + distance[dest].dist);
    }


    private static final int infinity = Integer.MAX_VALUE;
    private static final int undefined = Integer.MIN_VALUE;

    private static void initData(int[] distance, int[] predecessor, boolean[] processed) {
        Arrays.fill(distance, infinity);
        Arrays.fill(predecessor, undefined);
        Arrays.fill(processed, false);
    }

    private static void initData(int[] predecessor, PriorityQueue<Vertex> pq, Vertex[] vertexes, int source) {
        Arrays.fill(predecessor, undefined);
        for (int i = 0, max = predecessor.length; i < max; i++) {
            vertexes[i] = new Vertex(i, i == source ? 0 : infinity);
            pq.offer(vertexes[i]);
        }
    }

    private static void initData(int[] predecessor, TreeSet<Vertex> pq, Vertex[] vertexes, int source) {
        Arrays.fill(predecessor, undefined);
        for (int i = 0, max = predecessor.length; i < max; i++) {
            vertexes[i] = new Vertex(i, i == source ? 0 : infinity);
            pq.add(vertexes[i]);
        }
    }

    public static class Vertex {
        int num;
        int dist;

        public Vertex(int num, int dist) {
            this.num = num;
            this.dist = dist;
        }
    }

    private static int minVertex(int[] distance, boolean[] processed) {
        int min = undefined, minValue = infinity;
        for (int i = 0, max = distance.length; i < max; i++) {
            if (distance[i] < minValue && !processed[i]) {
                minValue = distance[i];
                min = i;
            }
        }
        return min;
    }

    private static void addNeighbors(int[][] graph, int vertex, int[] distance, int[] predecessor) {
        try {
            for (int i = 0, graphSize = graph.length; i < graphSize; i++) {
                if (graph[vertex][i] > 0) {//tzn, że jest krawędź pomiędzy tymi wierzchołkami
                    if (distance[i] > distance[vertex] + graph[vertex][i]) {//obecny dystans jest wiekszy niz nowy
                        distance[i] = distance[vertex] + graph[vertex][i]; //wstawiamy nowy mniejszy dystans
                        predecessor[i] = vertex;//ustawiamy nowy poprzednik
                    }
                }
            }
        } catch (IndexOutOfBoundsException e) {
            System.out.println(Arrays.deepToString(graph));
            System.out.println(Arrays.toString(distance));
            System.out.println(Arrays.toString(predecessor));
        }
    }


    private static void addNeighbors(Vertex[][] graph, int vertex, int[] distance, int[] predecessor) {
        for (int i = 0, neighbours = graph[vertex].length; i < neighbours; i++) {
            if (distance[graph[vertex][i].num] > distance[vertex] + graph[vertex][i].dist) {//obecny dystans jest wiekszy niz nowy
                distance[graph[vertex][i].num] = distance[vertex] + graph[vertex][i].dist; //wstawiamy nowy mniejszy dystans
                predecessor[graph[vertex][i].num] = vertex;//ustawiamy nowy poprzednik
            }
        }

    }

    private static void addNeighbors(Vertex[][] graph, int vertex, int[] predecessor,
                                     PriorityQueue<Vertex> pq, Vertex[] vertexes) {
        for (int i = 0, neighbours = graph[vertex].length; i < neighbours; i++) {
            if (vertexes[graph[vertex][i].num].dist > vertexes[vertex].dist + graph[vertex][i].dist) {//obecny dystans jest wiekszy niz nowy
                predecessor[graph[vertex][i].num] = vertex;//ustawiamy nowy poprzednik

                pq.remove(vertexes[graph[vertex][i].num]);//wyciągamy z kolejki priorytetowej wieżchołek
                vertexes[graph[vertex][i].num].dist = vertexes[vertex].dist + graph[vertex][i].dist;//ustawiamy mu nowy mniejszy dystans
                pq.offer(vertexes[graph[vertex][i].num]);//wstawiamy go z powrotem z mniejszym dystansem
            }
        }
    }

    private static void addNeighbors(Vertex[][] graph, int vertex, int[] predecessor,
                                     TreeSet<Vertex> pq, Vertex[] vertexes) {
        for (int i = 0, neighbours = graph[vertex].length; i < neighbours; i++) {
            if (vertexes[graph[vertex][i].num].dist > vertexes[vertex].dist + graph[vertex][i].dist) {//obecny dystans jest wiekszy niz nowy
                predecessor[graph[vertex][i].num] = vertex;//ustawiamy nowy poprzednik

                pq.remove(vertexes[graph[vertex][i].num]);//wyciągamy z kolejki priorytetowej wieżchołek
                vertexes[graph[vertex][i].num].dist = vertexes[vertex].dist + graph[vertex][i].dist;//ustawiamy mu nowy mniejszy dystans
                pq.add(vertexes[graph[vertex][i].num]);//wstawiamy go z powrotem z mniejszym dystansem
            }
        }
    }

    private static void dikstraTable(int graph[][], int source, int dest) {
        final int graphSize = graph.length;
        int[] distance = new int[graphSize];
        int[] predecessor = new int[graphSize];
        boolean[] processed = new boolean[graphSize];
        initData(distance, predecessor, processed);
        distance[source] = 0;
        addNeighbors(graph, source, distance, predecessor);
        processed[source] = true;

        int minVertex;
        for (int i = 1; i < graphSize; i++) {
            minVertex = minVertex(distance, processed);
            addNeighbors(graph, minVertex, distance, predecessor);
            processed[minVertex] = true;
        }

        writeOutput(distance, predecessor, source, dest, "dikstraTable ");
    }

    private static void dikstraList(Vertex graph[][], int source, int dest) {
        int graphSize = graph.length;
        int[] distance = new int[graphSize];
        int[] predecessor = new int[graphSize];
        boolean[] processed = new boolean[graphSize];
        initData(distance, predecessor, processed);
        distance[source] = 0;
        addNeighbors(graph, source, distance, predecessor);
        processed[source] = true;

        int minVertex;
        for (int i = 1; i < graphSize; i++) {
            minVertex = minVertex(distance, processed);
            addNeighbors(graph, minVertex, distance, predecessor);
            processed[minVertex] = true;
        }

        writeOutput(distance, predecessor, source, dest, "dikstraList ");
    }

    private static void dikstraListPQ(Vertex graph[][], int source, int dest) {
        int graphSize = graph.length;
        int[] predecessor = new int[graphSize];
        Vertex[] vertexes = new Vertex[graphSize];
        PriorityQueue<Vertex> pq = new PriorityQueue<>(graphSize - 1, (Vertex v1, Vertex v2) -> v1.dist - v2.dist);
        initData(predecessor, pq, vertexes, source);
        addNeighbors(graph, source, predecessor, pq, vertexes);

        Vertex minVertexx = pq.poll();
        while (minVertexx != null) {
            addNeighbors(graph, minVertexx.num, predecessor, pq, vertexes);
            minVertexx = pq.poll();
        }

        writeOutput(vertexes, predecessor, source, dest, "dikstraListPQ ");
    }

    private static void dikstraListTS(Vertex graph[][], int source, int dest) {
        int graphSize = graph.length;
        int[] predecessor = new int[graphSize];
        Vertex[] vertexes = new Vertex[graphSize];
        TreeSet<Vertex> pq = new TreeSet<>((Vertex v1, Vertex v2) -> v1.dist - v2.dist);
        initData(predecessor, pq, vertexes, source);
        addNeighbors(graph, source, predecessor, pq, vertexes);

        Vertex minVertexx = pq.pollFirst();
        while (minVertexx != null) {
            addNeighbors(graph, minVertexx.num, predecessor, pq, vertexes);
            minVertexx = pq.pollFirst();
        }

        writeOutput(vertexes, predecessor, source, dest, "dikstraListTS ");
    }

    public static void main(String[] args) {
/*
        dikstraList(genListGraph(), 0, 4);
        dikstraListPQ(genListGraph(), 0, 4);
        dikstraTable(genTableGraph(), 0, 4);
        dikstraListTS(genListGraph(), 0, 4);
*/

        runOnce(100, false);
        runOnce(1000, false);
        runOnce(100000, false);
    }

    private static void runOnce(int graphSize, boolean dense) {
        long start, finish;
        /*int[][] tableGraph = genRandomTableGraph(graphSize, dense);
        start = System.currentTimeMillis();
        dikstraTable(tableGraph, source, destination);
        finish = System.currentTimeMillis();
        System.out.println("dikstraTable run took: " + (double) (finish - start) / 1000 / 60 + " minutes (" + (double) (finish - start) / 1000 + " seconds)");
*/

        //Vertex[][] listGraph = genListGraph(tableGraph);
        Vertex[][] listGraph = genRandomListGraph(graphSize, dense);

        int source = graphSize / 2, destination = graphSize - 1;

        start = System.currentTimeMillis();
        dikstraList(listGraph, source, destination);
        finish = System.currentTimeMillis();
        System.out.println("dikstraList run took: " + (double) (finish - start) / 1000 / 60 + " minutes (" + (double) (finish - start) / 1000 + " seconds)");

        start = System.currentTimeMillis();
        dikstraListPQ(listGraph, source, destination);
        finish = System.currentTimeMillis();
        System.out.println("dikstraListPQ run took: " + (double) (finish - start) / 1000 / 60 + " minutes (" + (double) (finish - start) / 1000 + " seconds)");

/*
        start = System.currentTimeMillis();
        dikstraListTS(listGraph, source, destination);
        finish = System.currentTimeMillis();
        System.out.println("dikstraListTS run took: " + (double) (finish - start) / 1000 / 60 + " minutes (" + (double) (finish - start) / 1000 + " seconds)");
*/
    }
}
