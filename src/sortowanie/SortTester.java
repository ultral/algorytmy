package sortowanie;


import java.util.*;

public class SortTester {

  private static class TestTime {
    int number = 0;
    long allTime = 0;
    long minTime = Long.MAX_VALUE;
    long maxTime = Long.MIN_VALUE;
    private void putMin(long min){
      minTime = (minTime>min?min:minTime);
    }
    private void putMax(long max){
      maxTime = (max>maxTime?max:maxTime);
    }
    public void addTime(long time){
      allTime += time;
      putMin(time);
      putMax(time);
      number++;
    }
  }

  private static Random randomizer = new Random(System.currentTimeMillis());
  private static int[] sourceTab;
  private static int[] copyTab;
  private static Map<String, TestTime> testTimes = new HashMap<String, TestTime>();
  private static Map<String, Integer> sortNames = new HashMap<String, Integer>();

  private static void printTime(String sortName, long start, long finish, int sortNo){
    System.out.println(sortName+"("+sortNo+") = "+(double)(finish-start)/1000+" seconds");
  }

  private static void printOutput(String sortName, long allTime, long minTime, long maxTime){
    System.out.println("#########################################################");
    //System.out.println(testsNumber+" "+sortName+" sorts took "+(double)allTime/1000+" seconds");
    System.out.println(sortName+" AVERAGE  run took "+(double)(allTime/testsNumber)/1000+" seconds ("+allTime/testsNumber+" milis)");
    System.out.println(sortName+" SHORTEST run took "+(double)(minTime)/1000+" seconds ("+minTime+" milis)");
    System.out.println(sortName+" LONGEST  run took "+(double)(maxTime)/1000+" seconds ("+maxTime+" milis)");
  }

  private static void makeSourceTab(){
    for (int i = 0; i<tabLength; i++){
      if (debug){
        sourceTab[i] = randomizer.nextInt(100)-50;
      } else {
        if (random) {
          sourceTab[i] = randomizer.nextInt();
        } else {
          sourceTab[i] = randomizer.nextInt(100)-50;
        }
      }
    }
  }

  private static final int numberOfBuckets = 19;
  @SuppressWarnings("unchecked")
  private static final Queue<Integer>[] buckets = new ArrayDeque[numberOfBuckets];
  static {
    // 10 for base 10 numbers
    for (int i=0; i<numberOfBuckets; i++) {
      buckets[i] = new ArrayDeque<Integer>();
    }
  }


  public static void internetRaxidSort() {

    int numberOfDigits = getMaxNumberOfDigits(); //Max number of digits
    int divisor = 1;
    int digit;
    for (int n=0; n<numberOfDigits; n++) {
      for (int d : copyTab) {
        digit = (d / divisor) % 10;
        buckets[digit+9].add(d);
      }
      int index = 0;
      for (Queue<Integer> bucket : buckets) {
        while (!bucket.isEmpty()) {
          int integer = bucket.remove();
          copyTab[index++] = integer;
        }
      }
      divisor *= 10;
    }


    for (int i=0; i<numberOfBuckets; i++) {
      buckets[i].clear();
    }

  }

  private static int getMaxNumberOfDigits() {
    int max = Integer.MIN_VALUE;
    int temp;
    for (int i : copyTab) {
      temp = (int)Math.log10(i)+1;
      if (temp>max) max=temp;
    }
    return max;
  }

  private static void testSorts(){
    sourceTab = new int[tabLength];
    copyTab = new int[tabLength];
    for (String sortName: sortNames.keySet()){
      testTimes.put(sortName, new TestTime());
    }
    List<String> keyList = new LinkedList<String>(sortNames.keySet());
    Collections.sort(keyList);
    for (int i = 0; i<testsNumber; i++){
      makeSourceTab();
      if (debug){
        System.out.printf("sourceTab='%s'\n", Arrays.toString(sourceTab));
      }
      for (String sortName: keyList) {
        testSort(sortName, i);
      }
    }
  }

  private static void writeOutput(){
    System.out.println("#########################################################");
    if (random) {
      System.out.println("Table (RANDOM keys) length was "+tabLength+" test runs "+testsNumber+" java version "+System.getProperty("java.version"));
    } else {
      System.out.println("Table (many similar keys) length was "+tabLength+" test runs "+testsNumber+" java version "+System.getProperty("java.version"));
    }
    for (String key: testTimes.keySet()){
      TestTime t = testTimes.get(key);
      printOutput(key, t.allTime, t.minTime, t.maxTime);
    }
  }

  private static void swap(int first, int second){
    int temp = copyTab[first];
    copyTab[first] = copyTab[second];
    copyTab[second] = temp;
  }

  private static void bubbleSort(){
    boolean swapped;
    int j_1;
    for (int i = 0; i<tabLength; i++){
      swapped = false;
      for (int j = tabLength-1; j>i; j--){
        j_1 = j-1;
        if (copyTab[j]<copyTab[j_1]){
          swap(j, j_1);
          swapped = true;
        }
      }
      if (!swapped) return;
    }
  }

  private static void insertionSortOp(int l, int r){
    int t, j;
    for (int i = l + 1; i<r; i++){
      t = copyTab[i];
      for (j = i; j > l && t < copyTab[j-1]; j--) {
        copyTab[j]=copyTab[j-1];
      }
      copyTab[j] = t;
    }

  }

  private static void insertionSortOpArrayCopy(int l, int r){
    int t, j;
    for (int i = l + 1; i<r; i++){
      t = copyTab[i]; j = i;
      while (j > l && t < copyTab[j-1]){j--;}
      if (i!=j){
        System.arraycopy(copyTab, j, copyTab, j+1, i-j);
        copyTab[j] = t;
      }
    }
  }

  private static void insertionSort(int l, int r){
    for (int i = l + 1; i<r; i++){
      for (int j = i; j > l && copyTab[j] < copyTab[j-1]; j--) {
        swap(j, j - 1);
      }
    }
  }

  private static void selectionSort(){
    int k;
    for (int i = 0; i<tabLength; i++){
      k = i;
      for (int j = i; j<tabLength; j++){
        if (copyTab[j] < copyTab[k]){
          k = j;
        }
      }
      swap(i,k);
    }
  }

  private static int quickSortConst = 55;
  private static void quickSortInsert(int l, int r){
    swap(l, getGoodSwap(l,r));
    int k = l;
    for (int i = l+1; i<r; i++){
      if (copyTab[i] < copyTab[l]) {
        swap(++k, i);
      }
    }
    swap(l, k);
    if (k-l>quickSortConst){
      quickSortInsert(l, k);
    } else {
      insertionSort(l,k);
    }

    if (r-k-1>quickSortConst){
      quickSortInsert(k + 1, r);
    } else {
      insertionSort(k+1,r);
    }
  }


  private static int getGoodSwap(int l, int r){
    int rand;

    if (r-l>300000) {
      int templ = getGoodSwap(l, ((r-l)/3)+l);
      rand = getGoodSwap(((r-l)/3)+l+1, ((r-l)/3*2)+l);
      r = getGoodSwap(((r-l)/3*2)+l+1, r);
      l = templ;
    } else {
      rand = l+1+randomizer.nextInt(r-2-l);
    }

    if (copyTab[l]>copyTab[r-1]){
      if (copyTab[r-1]>copyTab[rand]){
        return r-1;
      } else {
        if (copyTab[l]>copyTab[rand]){return rand;}
        else {return l;}
      }
    } else {
      if (copyTab[l]>copyTab[rand]){
        return l;
      } else {
        if (copyTab[r-1]>copyTab[rand]){return rand;}
        else {return r-1;}
      }
    }
  }

  private static void quickSort(int l, int r){
    if (l >= r) return;
    if (l+1 == r) return;
    swap(l, l+randomizer.nextInt(r-1-l));
    int k = l;
    for (int i = l+1; i<r; i++){
      if (copyTab[i] < copyTab[l]) {
        swap(++k, i);
      }
    }
    swap(l, k);
    quickSort(l,k);
    quickSort(k+1,r);
  }

  private static int findLongest(LinkedList<Integer> sortTab){
    int max = 0;
    int maxLength = 0;
    for (int i = 0; i<tabLength; i++){
      max = Math.max(max, copyTab[i]);
      sortTab.addLast(copyTab[i]);
    }
    while (max>0){
      max = max/10;
      maxLength++;
    }
    return maxLength;
  }

  private static void bucketSort(){
    LinkedList<Integer> sortTab = new LinkedList<Integer>();
    int longestNum = findLongest(sortTab);
    LinkedList<Integer>[] bucketTab = new LinkedList[19];
    for (int i = 0; i<19; i++){
      bucketTab[i] = new LinkedList<Integer>();
    }

    for (int i = 0; i<longestNum; i++){
      bucketSort(bucketTab, i, sortTab);
    }

    for (int i = 0; i<tabLength; i++){
      copyTab[i] = sortTab.removeFirst();
    }
  }


  private static void bucketSort(LinkedList<Integer>[] bucketTab, int sortPos, LinkedList<Integer> sortTab){
    int temp;
    int sortPosDiv = (sortPos>0?10:1);
    for (int i = 1; i<sortPos; i++){
      sortPosDiv *= 10;
    }

    for (int i = 0; i<tabLength; i++){
      temp = (sortTab.getFirst()/sortPosDiv)%10;
      bucketTab[temp+9].addLast(sortTab.removeFirst());
    }

    LinkedList<Integer> list;
    for (int i = 0; i<19; i++){
      list = bucketTab[i];
      while (!list.isEmpty()) {
        sortTab.addLast(list.removeFirst());
      }
    }
  }

  private static int cutoff = 55;
  private static void quicksort(int[] tab, int l, int r){
    swap(r, getGoodSwap(l,r+1));
    int i = l-1, j = r, p = l-1, q = r; int v = tab[r];
    for (;;)
    {
      while (tab[++i] < v) ;
      while (v < tab[--j]) if (j == l) break;
      if (i >= j) break;
      swap(i, j);
      if (tab[i] == v) { p++; swap(p, i); }
      if (v == tab[j]) { q--; swap(j, q); }
    }
    swap(i, r); j = i-1; i = i+1;
    int k;
    for (k = l; k < p; k++, j--) swap(k, j);
    for (k = r-1; k > q; k--, i++) swap(i, k);
    if (j-l<cutoff){insertionSortOpArrayCopy(l, j+1);} else {quicksort(tab, l, j);}
    if (r-i<cutoff){insertionSortOpArrayCopy(i, r+1);} else {quicksort(tab, i, r);}
  }

  private static boolean debug = false;
  private static boolean random = true;
  private static boolean minOutput = true;
  private static int tabLength = 5000000;
  private static int testsNumber = 100;

  private static void initSorts(){
    if (debug){
      tabLength = 25;
      testsNumber = 1;
    }
    //nlogn sorts
    sortNames.put("java build in sort", 1);
    //sortNames.put("quick sort, insert", 5);
    sortNames.put("partQuickSort2    ", 12);
    //sortNames.put("pure quick sort   ", 6);

    //n sorts
    //sortNames.put("internetRaxid sort", 8);
    //sortNames.put("bucket sort       ", 7);

    //n^2 sorts
    //sortNames.put("bubble sort       ", 2);
    //sortNames.put("insertion sort    ", 3);
    //sortNames.put("insertion sort op ", 9);
    //sortNames.put("insertion sort op+", 10);
    //sortNames.put("selection sort    ", 4);
  }

  private static void testCorrect(String sortName){
    boolean ok = true;
    for (int i = 1; i<tabLength && ok; i++){
      if (copyTab[i-1]>copyTab[i]) {ok = false;}
    }
    if (!ok){
      System.out.printf(sortName+" failed!\n");
    }
  }

  private static void testSort(String sortName, int sortNo){
    System.arraycopy(sourceTab, 0, copyTab, 0, tabLength);
    int sortInt = sortNames.get(sortName);

    long start = System.currentTimeMillis();
    switch (sortInt){
      case 1 : Arrays.sort(copyTab); break;
      case 2 : bubbleSort(); break;
      case 3 : insertionSort(0, tabLength); break;
      case 4 : selectionSort(); break;
      case 5 : quickSortInsert(0, tabLength); break;
      case 6 : quickSort(0, tabLength); break;
      case 7 : bucketSort(); break;
      case 8 : internetRaxidSort(); break;
      case 9 : insertionSortOp(0, tabLength); break;
      case 10: insertionSortOpArrayCopy(0, tabLength); break;
      case 12: quicksort(copyTab, 0, copyTab.length-1); break;
    }
    long finish = System.currentTimeMillis();

    testCorrect(sortName);

    if (debug){
      System.out.printf(sortName+" finish ='%s'\n", Arrays.toString(copyTab));
    }
    testTimes.get(sortName).addTime(finish-start);
    if (!minOutput && !debug){
      printTime(sortName, start, finish, sortNo);
    }
  }

  public static void main(String[] args) {
    initSorts();
    testSorts();
    writeOutput();
  }
}

