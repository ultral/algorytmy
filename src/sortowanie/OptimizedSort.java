package sortowanie;

import java.util.Random;

/**
 *
 */
public class OptimizedSort {

  private static Random random = new Random(System.currentTimeMillis());

  /**
   * Exchanges two elements of the given tab
   *
   * @param tab    - table of elements
   * @param first  - index of first element to exchange
   * @param second - index od second element to exchange
   */
  private static void swap(int[] tab, int first, int second) {
    int temp = tab[first];
    tab[first] = tab[second];
    tab[second] = temp;
  }

  private static int median(int[] tab, int l, int r) {
    if (l <= r + 1) return l;
    int rand = l + 1 + random.nextInt(r - 1 - l);

    if (tab[l] > tab[r]) {
      if (tab[r] > tab[rand]) {
        return r;
      } else {
        if (tab[l] > tab[rand]) {
          return rand;
        } else {
          return l;
        }
      }
    } else {
      if (tab[l] > tab[rand]) {
        return l;
      } else {
        if (tab[r] > tab[rand]) {
          return rand;
        } else {
          return r;
        }
      }
    }
  }

  /**
   * Simple insertion sort could look like that:
   * <p/>
   * for (int i = l + 1; i < r; i++){
   * for (int j = i; j > l && tab[j] < tab[j-1]; j--) {
   * swap(j, j - 1);
   * }
   * }
   * <p/>
   * below is modified version with:
   * - inline changes (slightly faster than swap)
   * - clever mowing part of tab instead of swapping elements (less assignments)
   * <p/>
   * You can think of using System.arraycopy to move part of table so it can look like that
   * <p/>
   * private static void insertionSortArrayCopy(int l, int r){
   * int t, j;
   * for (int i = l + 1; i <= r; i++){
   * t = tab[i]; j = i;
   * while (j > l && t < tab[j-1]){j--;}
   * if (i!=j){
   * System.arraycopy(tab, j, tab, j+1, i-j);
   * tab[j] = t;
   * }
   * }
   * }
   * <p/>
   * but it is not obvious that it will always work as intended and it will be faster so I leave it as it is.
   *
   * @param tab - table of elements
   * @param l   - index of first element to sort
   * @param r   - index of last element to sort
   */
  private static void insertionSort(int[] tab, int l, int r) {
    int t, j;
    for (int i = l + 1; i <= r; i++) {
      t = tab[i];
      for (j = i; j > l && t < tab[j - 1]; j--) {
        tab[j] = tab[j - 1];
      }
      tab[j] = t;
    }
  }

  private static int cutoff = 55;

  /**
   * This is modified quickSort so if behaves ok when there is a lot of similar elements in table see:
   * <p/>
   * QUICKSORT IS OPTIMAL
   * Robert Sedgewick
   * Jon Bentley
   * <p/>
   * http://www.cs.princeton.edu/~rs/talks/QuicksortIsOptimal.pdf
   * <p/>
   * Additional we use cutoff technique to leave small array parts for insertionSort to handle.
   * <p/>
   * Also we process smaller part of table first so it assures that stack size is max O(log(n))
   * <p/>
   * And las but not least we use median of tre elements in every quick sort iteration.
   *
   * @param tab - table of elements
   * @param l   - index of first element to sort
   * @param r   - index of last element to sort
   */

  private static void quickSort(int[] tab, int l, int r) {
    int i = l - 1, j = r, p = l - 1, q = r;
    swap(tab, r, median(tab, l, r));
    int v = tab[r];
    for (; ; ) {
      while (tab[++i] < v) {
      }
      while (v < tab[--j]) if (j == l) break;
      if (i >= j) break;
      swap(tab, i, j);
      if (tab[i] == v) {
        p++;
        swap(tab, p, i);
      }
      if (v == tab[j]) {
        q--;
        swap(tab, j, q);
      }
    }
    swap(tab, i, r);
    j = i - 1;
    i++;
    int k;
    for (k = l; k < p; k++, j--) swap(tab, k, j);
    for (k = r - 1; k > q; k--, i++) swap(tab, i, k);
    //here we assure that stack size is max O(log(n))
    if (j - l < r - i) {
      if (j - l < cutoff) {
        insertionSort(tab, l, j);
      } else {
        quickSort(tab, l, j);
      }
      if (r - i < cutoff) {
        insertionSort(tab, i, r);
      } else {
        quickSort(tab, i, r);
      }
    } else {
      if (r - i < cutoff) {
        insertionSort(tab, i, r);
      } else {
        quickSort(tab, i, r);
      }
      if (j - l < cutoff) {
        insertionSort(tab, l, j);
      } else {
        quickSort(tab, l, j);
      }
    }

  }

  public void sort(int[] tab) {
    if (tab.length > cutoff) {
      quickSort(tab, 0, tab.length - 1);
    } else {
      insertionSort(tab, 0, tab.length - 1);
    }
  }
}
