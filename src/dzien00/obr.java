import java.io.*;
import java.util.Arrays;
import java.util.StringTokenizer;

public class obr {

  private static int x, y;
  private static char painting[][];
  private static long before, after;

  private static void readInput() {
    before = System.currentTimeMillis();
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    try {

      StringTokenizer st = new StringTokenizer(br.readLine());
      x = Integer.parseInt(st.nextToken());
      //System.out.printf("x='%d'\n", x);
      y = Integer.parseInt(st.nextToken());
      //System.out.printf("y='%d'\n", y);

      painting = new char[x][y];

      for (int i = 0; i < x; i++) {//ys to wiersze..:(
        st = new StringTokenizer(br.readLine());
        painting[i] = st.nextToken().toCharArray();
        //System.out.printf("board[%d]='%s'\n", i, Arrays.toString(painting[i]));
      }

    } catch (IOException e) {
      System.out.print("IOException: " + e.toString());
    }

    after = System.currentTimeMillis();
    System.out.println("Input: " + (after - before));

  }

  private static void writeOutput() {
    before = System.currentTimeMillis();
    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
    try {
      for (int i = 0; i < y; i++) {
        for (int j = x - 1; j > -1; j--) {
          bw.write(painting[j][i]);
        }
        bw.newLine();
      }
      bw.flush();
    } catch (IOException e) {
      System.out.print("IOException: " + e.toString());
    }
    after = System.currentTimeMillis();
    System.out.println("Output: " + (after - before));
  }

  private static void doMagic() {

  }

  public static void main(String[] args) {
    readInput();
    doMagic();
    writeOutput();
  }
}
