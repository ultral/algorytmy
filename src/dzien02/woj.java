import java.io.*;
import java.util.Arrays;
import java.util.StringTokenizer;

public class woj {

  private static int n, ilosc, nastSta, nastJas, dolSta, dolJas, karty;
  private static int taliaJas[];
  private static int taliaSta[];

  private static void readInput() {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    try {
      StringTokenizer st = new StringTokenizer(br.readLine());
      n = Integer.parseInt(st.nextToken());
      karty = n;
      taliaJas = new int[n];
      taliaSta = new int[n];
      st = new StringTokenizer(br.readLine());
      for (int i = 0; i < n; i++ ){
        taliaJas[i] = Integer.parseInt(st.nextToken());
      }
      st = new StringTokenizer(br.readLine());
      for (int i = 0; i < n; i++ ){
        taliaSta[i] = Integer.parseInt(st.nextToken());
      }
/*      System.out.printf("taliaJas='%s'\n", Arrays.toString(taliaJas));
      System.out.printf("taliaSta='%s'\n", Arrays.toString(taliaSta));*/
    } catch (IOException e) {
      System.out.print("IOException: " + e.toString());
    }

  }

  private static void writeOutput() {
    System.out.print(ilosc);
  }

  private static boolean max(){
    boolean ret = true;
    int i = 1;
    while (karty>i){
      i = i*2;
      ret = !ret;
    }
    return ret;
  }

  private static int wezSta(){
    int ret = taliaSta[nastSta];
    taliaSta[nastSta] = 0;
    nastSta = (nastSta + 1)%n;
    return ret;
  }

  private static int wezJas(){
    int ret = taliaJas[nastJas];
    taliaJas[nastJas] = 0;
    nastJas = (nastJas + 1)%n;
    return ret;
  }

  private static void dj(int i){
    taliaJas[dolJas] = i;
    dolJas = (dolJas + 1)%n;
  }

  private static void ds(int i){
    taliaSta[dolSta] = i;
    dolSta = (dolSta + 1)%n;
  }

  private static int maximum(int k1, int k2){
    if (k1 > k2) {
      return k1;
    } else {
      return k2;
    }
  }

  private static int minimum(int k1, int k2){
    if (k1 < k2) {
      return k1;
    } else {
      return k2;
    }
  }

  private static void dajJas(int k1, int k2){
    boolean max = max();
    if (max) {
      dj(maximum(k1, k2));
    } else {
      dj(minimum(k1,k2));
    }
  }

  private static void dajSta(int k1, int k2){
    boolean max = max();
    if (max) {
      ds(maximum(k1, k2));
    } else {
      ds(minimum(k1,k2));
    }
  }

  private static void doMagic() {
    nastJas = 0;
    nastSta = 0;
    dolJas = 0;
    dolSta = 0;
    int sta1, sta2, jas1, jas2;

    while (karty != 1){
      sta1=   wezSta(); sta2 = wezSta();
      jas1=   wezJas(); jas2 = wezJas();
      dajJas(sta1, sta2);
      dajSta(jas1, jas2);
      karty--;
/*      System.out.printf("taliaJas='%s'\n", Arrays.toString(taliaJas));
      System.out.printf("taliaSta='%s'\n", Arrays.toString(taliaSta));*/
    }

    ilosc = wezJas() - wezSta();

  }

  public static void main(String[] args) {
    readInput();
    doMagic();
    writeOutput();
  }
}
