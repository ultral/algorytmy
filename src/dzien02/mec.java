import java.io.*;
import java.util.Arrays;
import java.util.StringTokenizer;

public class mec {

  private static int zaw, mec, zaw2;
  private static boolean tak ;
  private static int dru[][];

  private static void readInput() {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    try {
      StringTokenizer st = new StringTokenizer(br.readLine());
      zaw = Integer.parseInt(st.nextToken());
      mec = Integer.parseInt(st.nextToken());
      zaw2 = zaw/2;
      int zawNum;
      dru = new int[zaw][mec];
      for (int i = 0; i < mec; i++ ){
        st = new StringTokenizer(br.readLine());
        for (int j = 0; j < zaw; j++ ){
          zawNum = Integer.parseInt(st.nextToken());
          if (j>=zaw2) {
            dru[zawNum-1][i] = 1;
          } else {
            dru[zawNum-1][i] = 0 ;
          }
        }
      }
/*      for (int i = 0; i < zaw; i++ ){
        System.out.printf("dru[%d]='%s'\n", i, Arrays.toString(dru[i]));
      }*/

    } catch (IOException e) {
      System.out.print("IOException: " + e.toString());
    }

  }

  private static void writeOutput() {
    if (tak) {
      System.out.print("TAK");
    } else {
      System.out.print("NIE");
    }
  }

  private static boolean takieSame(int pier, int drug){
    for (int i = 0; i< mec; i++){
      if (dru[pier][i] != dru[drug][i]) {
        return false;
      }
    }
    return true;
  }

  private static void doMagic() {
    for (int i = 0;i<zaw-1;i++){
      for (int j = i+1;j<zaw;j++){
        if (takieSame(i,j)){
          tak = false;
          return;
        }
/*        System.out.println(i+" != "+j);*/
      }
    }
    tak = true;
  }

  public static void main(String[] args) {
    readInput();
    doMagic();
    writeOutput();
  }
}
