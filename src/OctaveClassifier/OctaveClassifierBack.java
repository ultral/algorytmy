package OctaveClassifier;

import java.awt.geom.Point2D;
import java.util.*;

/**
 * Konrad Błachnio for TopCoder Marathon Match OctaveClassifier Contest.
 */
public class OctaveClassifierBack {

  private static final int FM = 1;//Y
  private static final int TT = 2;//X
  private static final int Both = 3;
  private static final int Undetermined = 4;
  private static final int Aberrant = 5;
  private static final String Conservative = "C";
  private static final String Liberal = "L";
  private static Random random = new Random(System.currentTimeMillis());
  Map<Integer, List<Point2Did>> conservativeStrategy = new HashMap<Integer, List<Point2Did>>(8);
  Map<Integer, List<Point2Did>> liberalStrategy = new HashMap<Integer, List<Point2Did>>(8);
  long start;
  private float xConstant = 1.5f;
  private float yConstant = 1.5f;
  private float xyConstant = 1.2f;
  public float mostMatchRange = 8f;
  public float setHeuristic = 8f;
  public float mostMatchRandomRange = 4f;

  private void normalize(List<Point2Did> l) {
    float maxX = Float.MIN_VALUE;
    float maxY = Float.MIN_VALUE;
    float minX = Float.MAX_VALUE;
    float minY = Float.MAX_VALUE;
    for (Point2Did p : l) {
      if (maxX < p.x) maxX = p.x;
      if (maxY < p.y) maxY = p.y;
      if (minX > p.x) minX = p.x;
      if (minY > p.y) minY = p.y;
    }

    float maxXminX = maxX - minX;
    float maxYminY = maxY - minY;
    int width = 100;
    for (Point2Did p : l) {
      p.setLocation((p.x - minX) * width / maxXminX, (p.y - minY) * width / maxYminY);
    }
  }

  private int classificationId(String classification) {
    if (classification.equals("TT")) return TT;
    if (classification.equals("FM")) return FM;
    if (classification.equals("Both")) return Both;
    if (classification.equals("Undetermined")) return Undetermined;
    if (classification.equals("Aberrant")) return Aberrant;

    throw new IllegalArgumentException("Zły typ argumentu: " + classification);
  }

  private void addClassificationPoint(LinkedList<Point2Did> pointList, String[] row) {
    pointList.addLast(new Point2Did(Float.valueOf(row[2]), Float.valueOf(row[3]), classificationId(row[5])));
  }

  private void moveToStrategy(LinkedList<Point2Did> pointList, String strategy) {
    Map<Integer, List<Point2Did>> strategyMap = null;
    if (strategy.equals(Conservative)) {
      strategyMap = conservativeStrategy;
    } else if (strategy.equals(Liberal)) {
      strategyMap = liberalStrategy;
    }

    Point2Did p;

    while (!pointList.isEmpty()) {
      p = pointList.removeFirst();
      strategyMap.get(p.getId()).add(p);
    }
  }

  private void train(String[] trainingData) {
    int trainSize = trainingData.length, rowId = 0, testId;
    LinkedList<Point2Did> pointList = new LinkedList<Point2Did>();
    String[] row;
    String strategy;
    boolean sameTest;

    for (int i = 1; i < 6; i++) {
      conservativeStrategy.put(i, new LinkedList<Point2Did>());
      liberalStrategy.put(i, new LinkedList<Point2Did>());
    }

    while (rowId < trainSize) {
      sameTest = true;
      row = trainingData[rowId].split(",");
      testId = Integer.parseInt(row[0]);
      strategy = row[4];
      pointList.clear();
      addClassificationPoint(pointList, row);
      rowId++;
      while (sameTest && rowId < trainSize) {
        row = trainingData[rowId].split(",");
        if (testId == Integer.parseInt(row[0])) {
          addClassificationPoint(pointList, row);
          rowId++;
        } else {
          sameTest = false;
        }
      }
      if (strategy.equals(Conservative)){
        normalize(pointList);
      }
      moveToStrategy(pointList, strategy);
    }

  }

  private boolean XGreater(Point2Did p) {
    return p.x > p.y * xConstant;
  }

  private boolean YGreater(Point2Did p) {
    return p.x * yConstant < p.y;
  }

  private boolean XYEqual(Point2Did p) {
    return Math.max(p.x, p.y) < xyConstant * Math.min(p.x, p.y);
  }

  private int classificationMatch(Point2Did testPoint, List<Point2Did> list, double range) {
    int match = 0;
    for (Point2Did p : list) {
      if (p.distance(testPoint) < range) match++;
    }
    return match;
  }

  private int mostMatchRandomHeuristic(Point2Did testPoint, String strategy) {
    int[] count = new int[5];
    Map<Integer, List<Point2Did>> currentStrategy;
    if (strategy.equals(Conservative)) {
      currentStrategy = conservativeStrategy;
    } else {
      currentStrategy = liberalStrategy;
    }

    int sum = 0;
    for (int i = 1; i <= 5 ; i++) {
      count[i-1] = classificationMatch(testPoint, currentStrategy.get(i), mostMatchRandomRange);
      sum += count[i-1];
    }
    if (count[0] > 2*count[1]){
      sum -= count[1];count[1] = 0;
    } else if (count[1] > 2*count[0]){
      sum -= count[0]; count[0] = 0;
    } else {
      sum -= count[0]; sum -= count[1]; count[0] = 0; count[1] = 0; count[4]++;sum++;
    }

    int rand = random.nextInt(sum);
    sum = 0;
    for (int i = 0; i < 5 ; i++) {
      sum += count[i];
      if (rand < sum) return i+1;
    }

    return 0;
  }

  private int mostMatchHeuristic(Point2Did testPoint, String strategy, double mostMatchRange) {
    int bestCount = Integer.MIN_VALUE, classification = Aberrant, currentCount;
    Map<Integer, List<Point2Did>> currentStrategy;
    if (strategy.equals(Conservative)) {
      currentStrategy = conservativeStrategy;
    } else {
      currentStrategy = liberalStrategy;
    }

    for (int i = 5; i > 0; i--) {
      currentCount = classificationMatch(testPoint, currentStrategy.get(i), mostMatchRange);
      if (currentCount > bestCount) {
        bestCount = currentCount;
        classification = i;
      }
    }

    return classification;
  }

  private int simpleHeuristic(Point2Did testPoint) {
    int classification;
    if (XGreater(testPoint)) {
      classification = TT;
    } else if (YGreater(testPoint)) {
      classification = FM;
    } else if (XYEqual(testPoint)) {
      classification = Both;
    } else {
      classification = Aberrant;
    }
    return classification;
  }

  private double calculateAvgDistance(double[][] distanceMatrix){
    double avgDistance = 0;
    int size = distanceMatrix.length;
    for (int i = 0; i < size; i++){
      for (int j = i+1; j < size; j++){
        avgDistance += distanceMatrix[i][j];
      }
    }

    return avgDistance/((size*(size-1))/2);
  }

  private double[][] calculateDistanceMatrix(Point2Did[] pointTab){
    int size = pointTab.length;
    double[][] distanceMatrix = new double[size][size];
    for (int i = 0; i < size; i++){
      for (int j = i+1; j < size; j++){
        distanceMatrix[i][j] = pointTab[i].distance(pointTab[j]);
      }
    }

    return distanceMatrix;
  }

  private int[] setHeuristic(LinkedList<Point2Did> pointList, String strategy) {
    int size = pointList.size(), classification = 0, i = 0;
    Point2Did[] pointTab = new Point2Did[size];
    double[][] distanceMatrix = calculateDistanceMatrix(pointList.toArray(pointTab));
    double avgDistance = calculateAvgDistance(distanceMatrix);
    int[] output = new int[size];

    for (Point2Did p : pointList) {
      classification = mostMatchHeuristic(p, strategy, avgDistance/setHeuristic);
      output[i++] = classification;
    }
    return output;
  }

  private int[] singleTest(LinkedList<Point2Did> pointList, String strategy) {
    int size = pointList.size(), i = 0, classification = 0;
    int[] output = new int[size];
    long now = System.currentTimeMillis();
    boolean fast = true;//((double)(now-start)/1000/60)>9.75;

/*    if (strategy.equals(Liberal)) {
      return setHeuristic(pointList, strategy);
    }*/

    if (fast) {
      for (Point2Did p : pointList) {
        classification = simpleHeuristic(p);
        output[i++] = classification;
      }
      return output;
    }

    for (Point2Did p : pointList) {
      if (strategy.equals(Conservative)) {
        classification = mostMatchHeuristic(p, strategy, mostMatchRange);
      } else if (strategy.equals(Liberal)) {
        if (((double)(now-start)/1000/60)>9 && random.nextBoolean()){
          classification = simpleHeuristic(p);
        } else {
          //classification = mostMatchHeuristic(p, strategy);
          classification = mostMatchRandomHeuristic(p, strategy);
        }
      }
      output[i++] = classification;
    }

    return output;
  }

  private void addPoint(LinkedList<Point2Did> pointList, String[] row) {
    pointList.addLast(new Point2Did(Float.valueOf(row[2]), Float.valueOf(row[3]), Integer.valueOf(row[1])));
  }

  private int[] testAll(String[] testData) {
    int testSize = testData.length, rowId = 0, outputId = 0, testId;
    int[] output = new int[testSize], singleOutput;
    LinkedList<Point2Did> pointList = new LinkedList<Point2Did>();
    String[] row;
    String currentStrategy;
    boolean sameTest;
    while (rowId < testSize) {
      pointList.clear();
      sameTest = true;
      row = testData[rowId].split(",");
      testId = Integer.parseInt(row[0]);
      addPoint(pointList, row);
      currentStrategy = row[4];
      rowId++;
      while (sameTest && rowId < testSize) {
        row = testData[rowId].split(",");
        if (testId == Integer.parseInt(row[0])) {
          addPoint(pointList, row);
          rowId++;
        } else {
          sameTest = false;
        }
      }
      if (currentStrategy.equals(conservativeStrategy)){
        normalize(pointList);
      }
      singleOutput = singleTest(pointList, currentStrategy);
      for (int classification : singleOutput) {
        output[outputId++] = classification;
      }
      //System.out.println("id: " + outputId + " / " + testSize);
    }

    return output;
  }

  public int[] classify(String[] trainingData, String[] testData) {
    start = System.currentTimeMillis();
    train(trainingData);
    return testAll(testData);
  }

  class Point2Did extends Point2D.Float {
    private int id;

    Point2Did(float x, float y, int id) {
      super(x, y);
      this.id = id;
    }

    public int getId() {
      return id;
    }

    public double distance(Point2Did point){
      return this.distance(point.getX(), point.getY());
    }
  }
}
