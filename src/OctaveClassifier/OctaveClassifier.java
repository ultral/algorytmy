package OctaveClassifier;

import java.awt.geom.Point2D;
import java.util.*;

/**
 * Konrad Błachnio for TopCoder Marathon Match OctaveClassifier Contest.
 */
public class OctaveClassifier {

  private static final int FM = 1;//Y
  private static final int TT = 2;//X
  private static final int Both = 3;
  private static final int Undetermined = 4;
  private static final int Aberrant = 5;
  private static final String Conservative = "C";
  private static final String Liberal = "L";
  private static Random random = new Random(System.currentTimeMillis());
  public float cHeuristicDivider = 4.25f;
  public int cHeuristicBigSet = 1;
  public int cHeuristicClosePoints = 8;
  public int lHeuristicBigSet = 5;
  public float lHeuristicDivider = 4.5f;
  Map<Integer, List<PointSet<Point2Did>>> conservativeStrategy = new HashMap<Integer, List<PointSet<Point2Did>>>();
  Map<Integer, List<PointSet<Point2Did>>> liberalStrategy = new HashMap<Integer, List<PointSet<Point2Did>>>();
  long start;
  private float xConstant = 1.2f;
  private float xyConstant = xConstant;
  private float yConstant = xConstant;

  private void processTestCase(LinkedList<Point2Did> pointList, String strategy, int testId) {
    //List<PointSet<Point2Did>> groups = calculateGroups(pointList, strategy);
    List<PointSet<Point2Did>> outputGroups = new LinkedList<PointSet<Point2Did>>();
    List<PointSet<Point2Did>> predefinedGroups = new LinkedList<PointSet<Point2Did>>();
    PointSet<Point2Did> TTSet = new PointSet<Point2Did>(), FMSet = new PointSet<Point2Did>();
    PointSet<Point2Did> USet = new PointSet<Point2Did>(), BothSet = new PointSet<Point2Did>();
    PointSet<Point2Did> ASet = new PointSet<Point2Did>();
    TTSet.classification = TT; FMSet.classification = FM; USet.classification = Undetermined;
    BothSet.classification = Both; ASet.classification = Aberrant;
    predefinedGroups.add(TTSet);
    predefinedGroups.add(FMSet);
    predefinedGroups.add(USet);
    predefinedGroups.add(BothSet);
    predefinedGroups.add(ASet);
    for (Point2Did p : pointList) {
      switch (p.id) {
        case TT:
          TTSet.add(p);
          break;
        case FM:
          FMSet.add(p);
          break;
        case Both:
          BothSet.add(p);
          break;
        case Undetermined:
          USet.add(p);
          break;
        case Aberrant:
          ASet.add(p);
          break;
      }
    }
    for (PointSet<Point2Did> ps : predefinedGroups) {
      if (ps.size() > 0) {
        normalizeAndCalculateWeight(ps);
        outputGroups.add(ps);
      }
    }

    if (strategy.equals(Conservative)) {
      conservativeStrategy.put(testId, outputGroups);
    } else {
      liberalStrategy.put(testId, outputGroups);
    }
  }

  private int classificationId(String classification) {
    if (classification.equals("TT")) return TT;
    if (classification.equals("FM")) return FM;
    if (classification.equals("Both")) return Both;
    if (classification.equals("Undetermined")) return Undetermined;
    if (classification.equals("Aberrant")) return Aberrant;

    throw new IllegalArgumentException("Zły typ argumentu: " + classification);
  }

  private void addClassificationPoint(LinkedList<Point2Did> pointList, String[] row) {
    pointList.addLast(new Point2Did(Float.valueOf(row[2]), Float.valueOf(row[3]), classificationId(row[5])));
  }

  private void train(String[] trainingData) {
    int trainSize = trainingData.length, rowId = 0, testId;
    LinkedList<Point2Did> pointList = new LinkedList<Point2Did>();
    String[] row;
    String strategy;
    boolean sameTest;

    while (rowId < trainSize) {
      sameTest = true;
      row = trainingData[rowId].split(",");
      testId = Integer.parseInt(row[0]);
      strategy = row[4];
      pointList.clear();
      addClassificationPoint(pointList, row);
      rowId++;
      while (sameTest && rowId < trainSize) {
        row = trainingData[rowId].split(",");
        if (testId == Integer.parseInt(row[0])) {
          addClassificationPoint(pointList, row);
          rowId++;
        } else {
          sameTest = false;
        }
      }
      processTestCase(pointList, strategy, testId);
    }

  }

  private boolean XGreater(Point2Did p, float maxX, float minX, float maxY, float minY) {
    //return  XGreaterSet(p,maxX,minY,maxY,minY);
    return (p.x > p.y * xConstant) && (p.x > (maxX - minX) / 2);
  }

  private boolean YGreater(Point2Did p, float maxX, float minX, float maxY, float minY) {
    //return  YGreaterSet(p,maxX,minY,maxY,minY);
    return (p.x * yConstant < p.y) && (p.y > (maxY - minY) / 2);
  }

  private boolean pointsClose(Point2Did p, Point2Did p2, float maxX, float minX, float maxY, float minY) {
    double distance = p.distance(p2);
    double maxDistance = Math.sqrt((maxX - minX) * (maxY - minY)) / cHeuristicClosePoints;
    return distance < maxDistance;
  }

  private boolean XYEqual(Point2Did p, float maxX, float minX, float maxY, float minY) {
    return (Math.max(p.x, p.y) < xyConstant * Math.min(p.x, p.y))
        && (p.x > (maxX - minX) / 2) && (p.y > (maxY - minY) / 2);
  }

  private boolean XYSmall(Point2Did p, float maxX, float minX, float maxY, float minY) {
    //return  XYSmallSet(p,maxX,minY,maxY,minY);
    return (p.x < (maxX - minX) / 2) && (p.y < (maxY - minY) / 2);
  }

  private Map<String, Float> calculateStatistics(LinkedList<Point2Did> pointList) {
    Map<String, Float> stat = new HashMap<String, Float>();
    int size = pointList.size(), i = 0;
    float[] x = new float[size];
    float[] y = new float[size];

    for (Point2Did p : pointList) {
      x[i] = p.x;
      y[i] = p.y;
      i++;
    }
    Arrays.sort(x);
    Arrays.sort(y);
    stat.put("MaxX", x[size - 1]);
    stat.put("MaxY", y[size - 1]);
    stat.put("MinX", x[0]);
    stat.put("MinY", y[0]);
    stat.put("MedianX", x[(size / 2)]);
    stat.put("MedianY", y[(size / 2)]);

    return stat;
  }

  private int[] simpleHeuristic(LinkedList<Point2Did> pointList, String strategy) {
    Map<String, Float> stat = calculateStatistics(pointList);
    float maxX = stat.get("MaxX"), maxY = stat.get("MaxY"), minX = stat.get("MinX"), minY = stat.get("MinY");
    int[] output = new int[pointList.size()];
    int i = 0;
    for (Point2Did p : pointList) {
      if (XGreater(p, maxX, minX, maxY, minY)) {
        output[i++] = TT;
      } else if (YGreater(p, maxX, minX, maxY, minY)) {
        output[i++] = FM;
      } else if (XYEqual(p, maxX, minX, maxY, minY)) {
        output[i++] = Both;
      } else if (XYSmall(p, maxX, minX, maxY, minY)) {
        output[i++] = Undetermined;
      } else {
        output[i++] = Aberrant;
      }
    }

    return output;
  }

  private double calculateAvgDistance(double[][] distanceMatrix) {
    double avgDistance = 0;
    int size = distanceMatrix.length;
    for (int i = 0; i < size; i++) {
      for (int j = i + 1; j < size; j++) {
        avgDistance += distanceMatrix[i][j];
      }
    }

    return avgDistance / ((size * (size - 1)) / 2);
  }

  private double[][] calculateDistanceMatrix(Point2Did[] pointTab) {
    int size = pointTab.length;
    double[][] distanceMatrix = new double[size][size];
    for (int i = 0; i < size; i++) {
      for (int j = i + 1; j < size; j++) {
        distanceMatrix[i][j] = pointTab[i].distance(pointTab[j]);
      }
    }

    return distanceMatrix;
  }

  private void addGroups(int[] groupNum, int firstGroup, int secondGroup) {

    if (groupNum[secondGroup] == 0) {
      groupNum[secondGroup] = groupNum[firstGroup];   //point is not in any group
    } else if (groupNum[firstGroup] == groupNum[secondGroup]) {
      return; //points are in the same group
    } else { //points are in different groups we must connect groups
      int size = groupNum.length;
      int secondGroupId = groupNum[secondGroup];
      for (int i = 0; i < size; i++) {
        if (groupNum[i] == secondGroupId) {
          groupNum[i] = groupNum[firstGroup];
        }
      }
    }
  }

  private PointSet<Point2Did> normalizeAndCalculateWeight(PointSet<Point2Did> ps) {
    Point2Did wp = new Point2Did(0, 0, ps.size());
    Point2Did xp = new Point2Did(Float.MIN_VALUE, Float.MAX_VALUE, ps.size());
    Point2Did yp = new Point2Did(Float.MIN_VALUE, Float.MAX_VALUE, ps.size());
    for (Point2Did p : ps) {
      xp.x = Math.max(xp.x, p.x);
      xp.y = Math.min(xp.y, p.x);

      yp.x = Math.max(yp.x, p.y);
      yp.y = Math.min(yp.y, p.y);
    }
    wp.x = wp.x / wp.id;
    wp.y = wp.y / wp.id;
    ps.weightPoint = wp;
    ps.xPoint = xp;
    ps.yPoint = yp;

    float maxXminX = xp.x - xp.y;
    float minX = xp.y;
    float maxYminY = yp.x - yp.y;
    float minY = xp.y;
    int width = 100;
    xp.setLocation(Float.MIN_VALUE, Float.MAX_VALUE);
    yp.setLocation(Float.MIN_VALUE, Float.MAX_VALUE);
    for (Point2Did p : ps) {
      p.setLocation((p.x - minX) * width / maxXminX, (p.y - minY) * width / maxYminY);
      xp.x = Math.max(xp.x, p.x);
      xp.y = Math.min(xp.y, p.x);

      yp.x = Math.max(yp.x, p.y);
      yp.y = Math.min(yp.y, p.y);

      wp.x += p.x;
      wp.y += p.y;
    }
    wp.x = wp.x / wp.id;
    wp.y = wp.y / wp.id;

    return ps;
  }

  private List<PointSet<Point2Did>> calculateGroups(LinkedList<Point2Did> pointList, String strategy) {
    int size = pointList.size();
    Point2Did[] pointTab = new Point2Did[size];
    double[][] distanceMatrix = calculateDistanceMatrix(pointList.toArray(pointTab));
    double avgDistance = calculateAvgDistance(distanceMatrix);
    double distance;
    if (strategy.equals(Conservative)) {
      distance = avgDistance / cHeuristicDivider;
    } else {
      distance = avgDistance / lHeuristicDivider;
    }

    List<PointSet<Point2Did>> groups = new LinkedList<PointSet<Point2Did>>();
    int[] groupNum = new int[size];
    for (int i = 0; i < size; i++) {
      if (groupNum[i] == 0) {
        groupNum[i] = i + 1;
      }
      for (int j = i + 1; j < size; j++) {
        if (distanceMatrix[i][j] < distance) {
          addGroups(groupNum, i, j);
        }
      }
    }

    Map<Integer, PointSet<Point2Did>> groupMap = new HashMap<Integer, PointSet<Point2Did>>();
    PointSet<Point2Did> currentSet;

    int i = 0;
    for (Point2Did p : pointList) {
      currentSet = groupMap.get(groupNum[i]);
      if (currentSet == null) {
        currentSet = new PointSet<Point2Did>();
      }
      currentSet.add(p);
      groupMap.put(groupNum[i], currentSet);
      i++;
    }
    for (PointSet<Point2Did> ps : groupMap.values()) {
      groups.add(normalizeAndCalculateWeight(ps));
    }
    Collections.sort(groups);
    return groups;
  }

  private PointSet<Point2Did> findMaxXSet(List<PointSet<Point2Did>> groups, float maxX, float minX, float maxY, float minY) {
    PointSet<Point2Did> maxXSet = new PointSet<Point2Did>();
    Point2Did weightPoint = new Point2Did(Float.MIN_VALUE, Float.MIN_VALUE, Integer.MIN_VALUE);
    for (PointSet<Point2Did> ps : groups) {
      if (weightPoint.x < ps.xPoint.x /*&& ps.weightPoint.y < (maxY-minY)*3/7*/) {
        maxXSet = ps;
        weightPoint.setLocation(ps.xPoint.x, ps.weightPoint.y);
      }
    }

    List<PointSet<Point2Did>> maxXSets = new LinkedList<PointSet<Point2Did>>();
    maxXSets.add(maxXSet);
    for (PointSet<Point2Did> ps : groups) {
      if (pointsClose(weightPoint, ps.weightPoint, maxX, minX, maxY, minY)) {
        maxXSets.add(ps);
      }
    }

    groups.removeAll(maxXSets);
    for (PointSet<Point2Did> ps : maxXSets) {
      maxXSet.addAll(ps);
    }
    return maxXSet;
  }

  private PointSet<Point2Did> findMaxYSet(List<PointSet<Point2Did>> groups, float maxX, float minX, float maxY, float minY) {
    PointSet<Point2Did> maxYSet = new PointSet<Point2Did>();
    Point2Did weightPoint = new Point2Did(Float.MIN_VALUE, Float.MIN_VALUE, Integer.MIN_VALUE);
    for (PointSet<Point2Did> ps : groups) {
      //x is always bigger axis at yPoint this is why yPoint.x look at normalizeAndCalculateWeight
      if (weightPoint.y < ps.yPoint.x /*&& ps.weightPoint.x < (maxX-minX)*3/7*/) {
        maxYSet = ps;
        weightPoint.setLocation(ps.weightPoint.x, ps.yPoint.x);
      }
    }

    List<PointSet<Point2Did>> maxYSets = new LinkedList<PointSet<Point2Did>>();
    maxYSets.add(maxYSet);
    for (PointSet<Point2Did> ps : groups) {
      if (pointsClose(weightPoint, ps.weightPoint, maxX, minX, maxY, minY)) {
        maxYSets.add(ps);
      }
    }

    groups.removeAll(maxYSets);
    for (PointSet<Point2Did> ps : maxYSets) {
      maxYSet.addAll(ps);
    }
    return maxYSet;
  }

  private PointSet<Point2Did> findMaxUSet(List<PointSet<Point2Did>> groups, float maxX, float minX, float maxY, float minY) {
    PointSet<Point2Did> maxUSet = new PointSet<Point2Did>();
    Point2Did weightPoint = new Point2Did(Float.MIN_VALUE, Float.MIN_VALUE, Integer.MIN_VALUE);
    for (PointSet<Point2Did> ps : groups) {
      if (ps.weightPoint.y < weightPoint.y &&
          ps.weightPoint.x < weightPoint.x) {
        maxUSet = ps;
        weightPoint = ps.weightPoint;
      }
    }
    groups.remove(maxUSet);
    return maxUSet;
  }

  private PointSet<Point2Did> findMaxBothSet(List<PointSet<Point2Did>> groups, float maxX, float minX, float maxY, float minY) {
    PointSet<Point2Did> maxUSet = new PointSet<Point2Did>();
    Point2Did weightPoint = new Point2Did(Float.MIN_VALUE, Float.MIN_VALUE, Integer.MIN_VALUE);
    for (PointSet<Point2Did> ps : groups) {
      if (weightPoint.y < ps.weightPoint.y &&
          weightPoint.x < ps.weightPoint.x) {
        maxUSet = ps;
        weightPoint = ps.weightPoint;
      }
    }
    groups.remove(maxUSet);
    return maxUSet;
  }

  private List<PointSet<Point2Did>> cutoffSmallSets(List<PointSet<Point2Did>> groups, int bigSize) {
    LinkedList<PointSet<Point2Did>> bigGroups = new LinkedList<PointSet<Point2Did>>();

    for (PointSet<Point2Did> ps : groups) {
      if (bigGroups.size() < 4) {
        bigGroups.addLast(ps);
      } else if (ps.weightPoint.id > bigSize) {
        bigGroups.addLast(ps);
      }
    }

    return bigGroups;
  }

  private int[] generateOutput(int size, PointSet<Point2Did> TTSet, PointSet<Point2Did> FMSet,
                               PointSet<Point2Did> BothSet, PointSet<Point2Did> USet, LinkedList<Point2Did> pointList) {
    int[] output = new int[size];
    int i = 0, classification;
    for (Point2Did p : pointList) {
      if (TTSet.contains(p)) {
        classification = TT;
      } else if (FMSet.contains(p)) {
        classification = FM;
      } else if (BothSet.contains(p)) {
        classification = Both;
      } else if (USet.contains(p)) {
        classification = Undetermined;
      } else {
        classification = Aberrant;
      }
      output[i++] = classification;
    }

    return output;
  }

  private int[] conservativeSetHeuristic(LinkedList<Point2Did> pointList, String strategy) {
    Map<String, Float> stat = calculateStatistics(pointList);
    float maxX = stat.get("MaxX"), maxY = stat.get("MaxY"), minX = stat.get("MinX"), minY = stat.get("MinY");
    float medianX = stat.get("MedianX"), medianY = stat.get("MedianY");
    List<PointSet<Point2Did>> groups = calculateGroups(pointList, strategy);
    PointSet<Point2Did> TTSet , FMSet, USet, BothSet;
    groups = cutoffSmallSets(groups, cHeuristicBigSet);
    TTSet = findMaxXSet(groups, maxX, minX, maxY, minY);
    FMSet = findMaxYSet(groups, maxX, minX, maxY, minY);
    USet = findMaxUSet(groups, maxX, minX, maxY, minY);
    BothSet = findMaxBothSet(groups, maxX, minX, maxY, minY);
    return generateOutput(pointList.size(), TTSet, FMSet, BothSet, USet, pointList);
  }

  private double calculateSetsMatch(List<PointSet<Point2Did>> g1, List<PointSet<Point2Did>> g2){
    double matchValue = 0,s1Size, s2Size;
    for (PointSet<Point2Did> s1 : g1){
      for (PointSet<Point2Did> s2 : g2){
        s1Size = Math.sqrt((s1.xPoint.x-s1.xPoint.y)*(s1.yPoint.x-s1.yPoint.y));
        s2Size = Math.sqrt((s2.xPoint.x-s2.xPoint.y)*(s2.yPoint.x-s2.yPoint.y));
        matchValue += s1.weightPoint.id*s2.weightPoint.id/Math.max(s1.weightPoint.distance(s2.weightPoint),1)/
            (Math.max(s1Size,s2Size)/Math.min(s1Size,s2Size));
      }
    }
    return matchValue;
  }

  private List<PointSet<Point2Did>> findBestTrainingSet(List<PointSet<Point2Did>> groups, String strategy){
    double bestValue = Float.MIN_VALUE, currValue;
    Map<Integer, List<PointSet<Point2Did>>> currentStrategy;
    if (strategy.equals(Conservative)) {
      currentStrategy = conservativeStrategy;
    } else {
      currentStrategy = liberalStrategy;
    }
    List<PointSet<Point2Did>> currentBest = currentStrategy.get(0);
    List<PointSet<Point2Did>> g;
    int best = 0;
    for (int i : currentStrategy.keySet()){
      g = currentStrategy.get(i);
      currValue = calculateSetsMatch(groups, g);
      if (currValue>bestValue) {
        currentBest = g; best = i;
        bestValue = currValue;
      }
    }

    return currentBest;
  }

  private int classificationMatch(Point2Did testPoint, PointSet<Point2Did> set, double maxDistance) {
    int match = 0;
    for (Point2Did p : set) {
      if (p.distance(testPoint) < maxDistance) match++;
    }
    return match;
  }

  private int findClassification(Point2Did p, List<PointSet<Point2Did>> trainingGroup, double maxDistance){
    int classification = Aberrant, bestMatch = Integer.MIN_VALUE, currMatch;

    for (PointSet<Point2Did> ps : trainingGroup){
      currMatch = classificationMatch(p, ps, maxDistance);
      if (currMatch>bestMatch) {
        bestMatch = currMatch;
        classification = ps.classification;
      }
    }

    return classification;
  }

  private Map<Integer, PointSet<Point2Did>> generateOutputSets(LinkedList<Point2Did> pointList,
                                                              List<PointSet<Point2Did>> trainingGroup, String strategy){


    int size = pointList.size();
    Point2Did[] pointTab = new Point2Did[size];
    double[][] distanceMatrix = calculateDistanceMatrix(pointList.toArray(pointTab));
    double avgDistance = calculateAvgDistance(distanceMatrix);
    double distance;
    if (strategy.equals(Conservative)) {
      distance = avgDistance / cHeuristicDivider;
    } else {
      distance = avgDistance / lHeuristicDivider;
    }

    Map<Integer, PointSet<Point2Did>> output = new HashMap<Integer, PointSet<Point2Did>>();
    output.put(TT, new PointSet<Point2Did>());
    output.put(FM, new PointSet<Point2Did>());
    output.put(Both, new PointSet<Point2Did>());
    output.put(Undetermined, new PointSet<Point2Did>());
    output.put(Aberrant, new PointSet<Point2Did>());

    for (Point2Did p : pointList){
      output.get(findClassification(p,trainingGroup, distance)).add(p);
    }

    return output;
  }


  private int[] liberalSetHeuristic(LinkedList<Point2Did> pointList, String strategy) {
    Map<String, Float> stat = calculateStatistics(pointList);
    float maxX = stat.get("MaxX"), maxY = stat.get("MaxY"), minX = stat.get("MinX"), minY = stat.get("MinY");
    float medianX = stat.get("MedianX"), medianY = stat.get("MedianY");
    List<PointSet<Point2Did>> groups = calculateGroups(pointList, strategy);
    List<PointSet<Point2Did>> trainingGroup = findBestTrainingSet(groups, strategy);
    Map<Integer, PointSet<Point2Did>> oSet = generateOutputSets(pointList, trainingGroup, strategy);
    return generateOutput(pointList.size(), oSet.get(TT), oSet.get(FM), oSet.get(Both), oSet.get(Undetermined), pointList);
  }

  private int[] singleTest(LinkedList<Point2Did> pointList, String strategy) {
    long now = System.currentTimeMillis();
    boolean fast = ((double) (now - start) / 1000 / 60) > 9.75;

    if (fast) { //no time left do fast simple heuristic
      return simpleHeuristic(pointList, strategy);
    } else if (strategy.equals(Conservative)) {//process conservative heuristic
      return conservativeSetHeuristic(pointList, strategy);
    } else {//process liberal heuristic
      return liberalSetHeuristic(pointList, strategy);
    }

  }

  private void addPoint(LinkedList<Point2Did> pointList, String[] row) {
    pointList.addLast(new Point2Did(Float.valueOf(row[2]), Float.valueOf(row[3]), Integer.valueOf(row[1])));
  }

  private int[] testAll(String[] testData) {
    int testSize = testData.length, rowId = 0, outputId = 0, testId;
    int[] output = new int[testSize], singleOutput;
    LinkedList<Point2Did> pointList = new LinkedList<Point2Did>();
    String[] row;
    String currentStrategy;
    boolean sameTest;
    while (rowId < testSize) {
      pointList.clear();
      sameTest = true;
      row = testData[rowId].split(",");
      testId = Integer.parseInt(row[0]);
      addPoint(pointList, row);
      currentStrategy = row[4];
      rowId++;
      while (sameTest && rowId < testSize) {
        row = testData[rowId].split(",");
        if (testId == Integer.parseInt(row[0])) {
          addPoint(pointList, row);
          rowId++;
        } else {
          sameTest = false;
        }
      }
      singleOutput = singleTest(pointList, currentStrategy);
      for (int classification : singleOutput) {
        output[outputId++] = classification;
      }
      //System.out.println("id: " + outputId + " / " + testSize);
    }

    return output;
  }

  public int[] classify(String[] trainingData, String[] testData) {
    start = System.currentTimeMillis();
    train(trainingData);
    return testAll(testData);
  }

  class PointSet<Point2Did> extends HashSet<Point2Did> implements Comparable {
    Point2Did weightPoint;
    Point2Did xPoint;
    Point2Did yPoint;
    int classification = 0;

    @Override
    public int compareTo(Object o) {
      if (o instanceof PointSet) {
        if (this.size() > ((PointSet) o).size()) {
          return -1;
        }
        if (this.size() < ((PointSet) o).size()) {
          return 1;
        }
        return 0;
      }
      return 0;
    }
  }

  class Point2Did extends Point2D.Float {
    private int id;

    Point2Did(float x, float y, int id) {
      super(x, y);
      this.id = id;
    }

    public int getId() {
      return id;
    }

    public double distance(Point2Did point) {
      return this.distance(point.getX(), point.getY());
    }
  }
}
