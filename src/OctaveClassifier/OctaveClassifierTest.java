package OctaveClassifier;

import java.io.*;
import java.util.*;

/**
 *
 */
public class OctaveClassifierTest {

  private final static Map<Integer, String> nameMap = new TreeMap<Integer, String>();
  private static String[] trainingData;
  private static String[] truthData;
  private static String[] testData;
  private static boolean writeFail;

  private static final int FM = 1;//Y
  private static final int TT = 2;//X
  private static final int Both = 3;
  private static final int Undetermined = 4;
  private static final int Aberrant = 5;

  private static String name(int classification) {

    if (classification == TT) return "TT";
    if (classification == FM) return "FM";
    if (classification == Both) return "Both";
    if (classification == Undetermined) return "Undetermined";
    if (classification == Aberrant) return "Aberrant";
    return "NONE!";

  }

  private static void readInput(String strategy, Set<String> observationsSet) {
    File test = new File("D:\\praca\\horus\\workspace\\PA2012\\src\\OctaveClassifier\\Example1TestData.csv");
    File train = new File("D:\\praca\\horus\\workspace\\PA2012\\src\\OctaveClassifier\\Example1TrainingData.csv");
    File truth = new File("D:\\praca\\horus\\workspace\\PA2012\\src\\OctaveClassifier\\Example1GroundTruth.csv");
    LinkedList<String> fList = new LinkedList<String>();
    LinkedList<String> fList2 = new LinkedList<String>();
    String line ;
    String line2 ;
    String[] tokens;

    try{
      BufferedReader br = new BufferedReader(new FileReader(test));
      BufferedReader br2 = new BufferedReader(new FileReader(truth));
      while (((line = br.readLine()) != null)){
        tokens = line.split(",");
        line2 = br2.readLine();
        if ((observationsSet == null || observationsSet.contains(tokens[0]))
              && (strategy == null || strategy.equals(tokens[4]))) {
          fList.addLast(line);
          fList2.addLast(line2);
        }
      }
      br.close();
      testData = new String[fList.size()];
      fList.toArray(testData);
      fList.clear();
      br2.close();
      truthData = new String[fList2.size()];
      fList2.toArray(truthData);
      fList2.clear();

      br = new BufferedReader(new FileReader(train));
      while (((line = br.readLine()) != null)){
        fList.addLast(line);
      }
      br.close();
      trainingData = new String[fList.size()];
      fList.toArray(trainingData);
      fList.clear();
      br.close();
    } catch (FileNotFoundException e){
      System.out.print("FileNotFoundException: " + e.toString());
    } catch (IOException e) {
      System.out.print("IOException: " + e.toString());
    }

  }

  public static void test(String strategy, Set<String> observationsSet, Float cHeuristicDivider, Float lHeuristicDivider){
    long start = System.currentTimeMillis();
    OctaveClassifier oc = new OctaveClassifier();
    if (cHeuristicDivider != null) { oc.cHeuristicDivider = cHeuristicDivider;}
    if (lHeuristicDivider != null){ oc.lHeuristicDivider = lHeuristicDivider;}

    if (strategy == null) {strategy = "ALL";}
    if (observationsSet == null) {(observationsSet = new TreeSet<String>()).add("ALL");}

    System.out.println("Start test for cHeuristicDivider: " + oc.cHeuristicDivider + ", lHeuristicDivider: "+oc.lHeuristicDivider);
    System.out.println("Startegy: "+ strategy+ ", observations: "+Arrays.toString(observationsSet.toArray()));
    int match = 0;
    int[] output = oc.classify(trainingData, testData);

    long finish = System.currentTimeMillis();

    float worstCase = Float.MAX_VALUE;
    int currentTest = 0,  worstCaseId = 0, currentOk = 0, currentAll = 0;
    String [] line = testData[0].split(",");
    currentTest = Integer.parseInt(line[0]);
    for (int i = 0; i< output.length; i++){
      line = testData[i].split(",");
      if (currentTest != Integer.parseInt(line[0])){
        if ((float)currentOk/currentAll<worstCase){worstCaseId = currentTest; worstCase = (float)currentOk/currentAll;}
        currentTest = Integer.parseInt(line[0]);
        currentOk = 0;
        currentAll = 0;
      }
      if (output[i] == (Integer.parseInt(truthData[i])+1)) {
        match++;
        currentOk++;
      } else if (writeFail) {
        System.out.println("data: "+testData[i]+" output:"+name(output[i])+" truth:"+name(Integer.parseInt(truthData[i])+1));
      }
      currentAll++;

    }
    if ((float)currentOk/currentAll<worstCase){worstCaseId = currentTest; worstCase = (float)currentOk/currentAll;}


    System.out.println("worst case id: " + worstCaseId+",  "+worstCase*100+" %");
    System.out.println("match: " + match+"/"+output.length+" "+((double)(match)/(output.length))*100+" %");
    System.out.println("run took: "+(double)(finish-start)/1000/60+" minutes ("+(double)(finish-start)/1000+" seconds)");
  }

  public static void main(String[] args) {

    nameMap.put(1, "FM");
    nameMap.put(2, "TT");
    nameMap.put(3, "Both");
    nameMap.put(4, "Undetermined");
    nameMap.put(5, "Aberrant");
    Set<String> observations = null;
    String strategy = null ;
    (observations = new TreeSet<String>()).add("4420");
    strategy = "L" ;
    readInput(strategy,observations);
    writeFail = true;
    test(strategy, observations, null, null);


    /*strategy = "L" ;
    readInput(strategy, observations);
    test(strategy, observations, 4.25f);*/
  }
}
