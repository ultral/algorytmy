package ITPW;
import java.io.*;
import java.util.*;

class MyPoint {
  public int x;
  public int y;

  MyPoint(){
  }

  MyPoint(int x, int y){
    this.x = x;
    this.y = y;
  }
}

class FlowPoint extends MyPoint{
  public int value;

  FlowPoint(int x, int y, int val){
    super(x, y);
    this.value = val;
  }
}

public class Gracz implements Runnable {

  private static char[][] cloneBoard(char board[][]){
    char boardClone[][] = board.clone();

    for (int i = 0; i < xs; i++){
      boardClone[i] = board[i].clone();
    }
    return boardClone;
  }

  //liczy tablicÄ™ kar za to, Å¼e jestesmy koÅ‚o muru
  private static void countTouchingWalls(){

    int wallNo;
    for (int x = 1; x < xs-1; x++)
      for (int y = 1; y < ys-1; y++){
        wallNo = 0;
        for (int i = -1; i <= 1; i++)
          for (int j = -1; j <= 1; j++)
            if (board[x+i][y+j] == '#')//mÃ³r
              wallNo+=WALLPENALTY;

        touchingWalls[x][y] = wallNo;
      }
  }

  private static void countTouchingPawns(int pawnStandingPenalty, int divider, int touchPawnsPenalty){
    int pawnsNo;
    for (int x = 1; x < xs-1; x++)
      for (int y = 1; y < ys-1; y++){
        pawnsNo = 0;
        for (int i = -1; i <= 1; i++)
          for (int j = -1; j <= 1; j++)
            if (pawns[x+i][y+j] > GRASS)
              pawnsNo+=touchPawnsPenalty;

        if (pawns[x][y] > GRASS) pawnsNo+=pawnStandingPenalty + pawns[x][y]/divider;
        touchingPawns[x][y] = pawnsNo;
      }
  }

  private static void countEnemyTouchingPawns(int penalty){
    int pawnsNo;
    for (int x = 1; x < xs-1; x++)
      for (int y = 1; y < ys-1; y++){
        pawnsNo = 0;
        for (int i = -1; i <= 1; i++)
          for (int j = -1; j <= 1; j++)
            if (pawns[x+i][y+j] < WALL)
              pawnsNo+=(penalty/10)+1;//nie checmy siÄ™ stykaÄ‡ z Å¼adnym przeciwnikiem w czasie ucieczki

        if (pawns[x][y] < WALL) pawnsNo+=penalty;
        touchingPawns[x][y] = pawnsNo;
      }
  }

  //daje pustÄ… tablicÄ™ intÃ³w wielkoÅ›ci tablicy gry
  private static int[][] giveEmpty(){
    int emptyArr[][] = emptyArray.clone();

    for (int i = 0; i < xs; i++){
      emptyArr[i] = emptyArray[i].clone();
    }
    return emptyArr;
  }

  public static void main(String[] args) {
    (new Gracz()).run();
  }

  private static void ok() {
    System.out.printf("=\n\n");
  }

  private static boolean is_small(char dyw){
    return (dyw>='a' && dyw<='z');
  }

  private static void makePawns(){
    for (int i = 0; i < xs; i++){
      for (int j = 0; j < ys; j++){
        switch (board[i][j]) {
          case '.': pawns[i][j] = GRASS; break;
          case '#': pawns[i][j] = WALL; break;
          default:
            if (is_small(board[i][j]) == myDivSizeSmall)
              pawns[i][j] = lives;
            else
              pawns[i][j] = (-lives) + WALL;
        }
      }
    }
  }

  private static void error() {
    System.out.printf("?\n\n");
  }
  private static int genDx(int v){
    return v%2==0?-1:1;
  }
  private static int genDy(int v){
    return v<2?-1:(v<4?1:(v<6?-1:1));
  }
  private static int genP(int v){
    return v<4?0:1;
  }
  //zmieniamy stan planszy sciagniete z arbitra
  private static void play(char dyw, int attack, int x, int y, int dx, int dy, int p, char board[][], int pawns[][]){

    boolean my_move = is_small(dyw) == myDivSizeSmall;

    final MyPoint cel = giveMyPoint(x, y);
    final MyPoint cel2 = giveMyPoint();
    final MyPoint cel3 = giveMyPoint();
    if (p==1) {
      cel2.x=x; cel2.y=y+dy; cel3.x = x+dx; cel3.y = y;
    } else {
      cel2.x=x+dx; cel2.y=y;cel3.x=x;cel3.y=y+dy;
    }

    Comparator<MyPoint> comp = new Comparator<MyPoint>(){
      int dist(MyPoint p1, MyPoint p2) {
        return (p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y);
      }
      public int compare(MyPoint p1, MyPoint p2) {
        int roz = dist(p1, cel)-dist(p2, cel);
        if (roz!=0)
          return roz;
        roz = dist(p1, cel2)-dist(p2, cel2);
        if (roz!=0)
          return roz;
        roz = dist(p1, cel3)-dist(p2, cel3);
        return roz;
      }
    };

    SortedSet<MyPoint> tmpPawns = new TreeSet<MyPoint>(comp);
    for (int xt = 1; xt<xs1; xt++)
      for (int yt = 1; yt<ys1; yt++)
        if (board[xt][yt]==dyw)
          tmpPawns.add(giveMyPoint(xt, yt));

    SortedSet<MyPoint> moves = new TreeSet<MyPoint>(comp);
    SortedSet<MyPoint> attacks = new TreeSet<MyPoint>(comp);
    int field;
    MyPoint move;
    for (MyPoint pawn : tmpPawns) {
      moves.clear();
      attacks.clear();
      for (int xd = -1; xd<=1; xd++)
        for (int yd = -1; yd<=1; yd++) {
          if ((field = pawns[pawn.x+xd][pawn.y+yd])==GRASS){
            moves.add(giveMyPoint(pawn.x+xd, pawn.y+yd));
          } else if (field != WALL) {
            if (my_move && field < 0){
              attacks.add(giveMyPoint(pawn.x+xd, pawn.y+yd));
            }
            if (!my_move && field > 0){
              attacks.add(giveMyPoint(pawn.x+xd, pawn.y+yd));
            }
          }
        }

      moves.add(giveMyPoint(pawn.x,pawn.y));

      if (attacks.isEmpty()) move = moves.first();
      else
        //attack prefered
        if (attack==1) {
          move = attacks.first();
        } else {//normal move
          moves.add(attacks.first());
          attacks.remove(attacks.first());
          move = moves.first();
        }

      if (move == null) continue;//no move to make
      if (move.x == pawn.x && move.y == pawn.y) continue;//no move to make
      makeMove(pawn, move, board, pawns);

      //recycle moves from attacks
      while (!attacks.isEmpty()){
        move = attacks.first();
        attacks.remove(move);
        emptyMyPointList.addFirst(move);
      }//recycle moves from moves
      while (!moves.isEmpty()){
        move = moves.first();
        moves.remove(move);
        emptyMyPointList.addFirst(move);
      }
    }//recycle moves from tmpPawns
    while (!tmpPawns.isEmpty()){
      move = tmpPawns.first();
      tmpPawns.remove(move);
      emptyMyPointList.addFirst(move);
    }
  }

  //wykonuje podany ruch na planszy z pola startowego na pole docelowe (musi byc obok!)
  private static void makeMove(MyPoint start, MyPoint dst, char board[][], int pawns[][]){
    if (pawns[dst.x][dst.y] != 1 && pawns[dst.x][dst.y] != -2 && pawns[dst.x][dst.y] != GRASS) {//attack wont kill
      if (pawns[dst.x][dst.y] > 0) pawns[dst.x][dst.y]--;
      else pawns[dst.x][dst.y]++;
    } else {//attack will kill or normal move it is the same
      board[dst.x][dst.y] = board[start.x][start.y];
      board[start.x][start.y] = '.';
      pawns[dst.x][dst.y] = pawns[start.x][start.y];
      pawns[start.x][start.y] = GRASS;
    }
  }

  private static int emptyArray[][]; //pionki
  private static boolean myDivSizeSmall;
  private static final int GRASS = 0;
  private static final int WALL = -1;
  private static int lives;
  private static int rounds;
  private static char flowBoard[][]; //plansza
  private static char board[][]; //plansza
  private static int flowTable[][]; //plansza
  private static int pawns[][]; //pionki
  private static int touchingWalls[][]; //ile mÃ³rÃ³w styka siÄ™ z danym polem
  private static int touchingPawns[][]; //ile moich pionkow styka siÄ™ z danym polem
  private static int xs, ys, xs1, ys1;
  private static Random rnd = new Random(System.currentTimeMillis());
  private static final LinkedList<MyPoint> emptyLinkedList = new LinkedList<MyPoint>();
  private static final LinkedList<MyPoint> emptyMyPointList = new LinkedList<MyPoint>();
  private static final LinkedList<FlowPoint> emptyFlowPointList = new LinkedList<FlowPoint>();
  private static int dx, dy, p, at;
  private static int PLANSZA = 0;
  private static final int SIEK = 1;
  private static final int ITPW = 2;
  private static final int JEZ = 3;
  private static final int LAB = 4;
  private static final int LAS = 5;
  private static final int OTO = 6;
  private static final int POLE = 7;
  private static final int ZW = 8;


  private static LinkedList<MyPoint> giveLinkedList(){
    return (LinkedList<MyPoint>) emptyLinkedList.clone();
  }

  private static MyPoint giveMyPoint(){
    if (emptyMyPointList.isEmpty()) return new MyPoint();
    else {
      return emptyMyPointList.removeFirst();
    }
  }

  private static MyPoint giveMyPoint(int x, int y){
    if (emptyMyPointList.isEmpty()) return new MyPoint(x, y);
    else {
      MyPoint p = emptyMyPointList.removeFirst();
      p.x = x; p.y = y;
      return p;
    }
  }

  private static FlowPoint giveFlowPoint(int x, int y, int val){
    if (emptyFlowPointList.isEmpty()) return new FlowPoint(x, y, val);
    else {
      FlowPoint fp = emptyFlowPointList.removeFirst();
      fp.x = x; fp.y = y; fp.value = val;
      return fp;
    }
  }

  private static MyPoint prepareFlowBoard(char div){
    int ret = 0, ret2 = 0;
    for (int x = 1; x < xs1; x++)
      for (int y = 1; y < ys1; y++){
        if (board[x][y] == div) {//to jest pionek z naszej dywizji

          flowBoard[x][y] = div;
          ret++;

          label: for (int i = -1; i <= 1; i++)
            for (int j = -1; j <= 1; j++)
              if (pawns[x+i][y+j] < WALL){//pionek przeciwnika
                //nasz pionek siÄ™ styka z przeciwnikiem wiÄ™c nie checmy go wliczaÄ‡ do algorytmu
                //znajdyjÄ…cego scieÅ¼kÄ™, moÅ¼na za niego wstawiÄ‡ znak zapytania
                flowBoard[x][y] = '?';
                ret2++;
                break label;
              }
        } else if (pawns[x][y] < WALL )
          flowBoard[x][y] = '!';
        else flowBoard[x][y] = board[x][y];
      }

    return giveMyPoint(ret,ret2);
  }

  private static final HashMap<Character, MyPoint> runAwayPoint = new HashMap<Character, MyPoint>(4);

  private static void prepareRetreatFlowBoard(char div){
    MyPoint p;

    if (runAwayPoint.containsKey(div))
      p = runAwayPoint.get(div);
    else {
      p = giveMyPoint(1,1);
      runAwayPoint.put(div,p);
    }

    for (int x = 1; x < xs1; x++)
      System.arraycopy(board[x],1,flowBoard[x],1,ys1-1);
    /*for (int y = 1; y < ys1; y++)
  flowBoard[x][y] = board[x][y];*/

    if (p.x == 1 && p.y == 1){
      for (int x = 1; x < xs1; x++)
        for (int y = 1; y < ys1; y++)
          if (div == board[x][y]) {//to jest pionek nie z naszej dywizji do niego biegniemy
            if ((x-1<5  && y-1<5)||(xs1-x<5 && ys1-x<5)){
              flowBoard[1][ys1-1]='!';flowBoard[xs1-1][1]='!';
              p.x = 2;
            } else {
              flowBoard[1][1]='!';flowBoard[xs1-1][ys1-1]='!';
            }
          }
    } else {
      for (int x = 1; x < xs1; x++)
        for (int y = 1; y < ys1; y++)
          if (div == board[x][y]) {//to jest pionek nie z naszej dywizji do niego biegniemy
            if ((x-1<5 && ys1-y<5)||(xs1-x<5 && y-1<5)){
              flowBoard[1][1]='!';flowBoard[xs1-1][ys1-1]='!';
              p.x = 1;
            } else {
              flowBoard[1][ys1-1]='!';flowBoard[xs1-1][1]='!';
            }
          }
    }
  }

  private static void zeroFlowTable(){
    for (int x = 1; x < xs1; x++)
      for (int y = 1; y < ys1; y++)
        if (!(board[x][y] == '#'))
          flowTable[x][y] = 0;
  }

  private static int countValue(int[][] pawns){
    int ret = 0, li;

    for (int x = 1; x<xs1; x++)
      for (int y = 1; y<ys1; y++)
        if ((emptyArray[x][y]) > 0){
          li = Math.min(lives + pawns[x][y] + 1 + emptyArray[x][y], lives);
          ret+= li*li;

          emptyArray[x][y]=0;
        }

    return ret;
  }

  //zakÅ‚adam, Å¼e ruszam swoje.. i wyliczam najlepszy ruch z 8 moÅ¼liwych..
  //zwracam wartoÅ›c na pierwszej zmiennej i typ ruchu na 2
  private static MyPoint myAttackValue(char div, int x, int y, char[][] board, int[][] pawns){
    final MyPoint cel = giveMyPoint(x, y);
    final MyPoint cel2 = giveMyPoint();
    final MyPoint cel3 = giveMyPoint();
    int tmpAttackValue;
    MyPoint ret = giveMyPoint();
    ret.x = Integer.MIN_VALUE;
    MyPoint p = null, p2;

    Comparator<MyPoint> comp = new Comparator<MyPoint>(){
      int dist(MyPoint p1, MyPoint p2) {
        return (p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y);
      }
      public int compare(MyPoint p1, MyPoint p2) {
        int roz = dist(p1, cel)-dist(p2, cel);
        if (roz!=0)
          return roz;
        roz = dist(p1, cel2)-dist(p2, cel2);
        if (roz!=0)
          return roz;
        roz = dist(p1, cel3)-dist(p2, cel3);
        return roz;
      }
    };

    SortedSet<MyPoint> tmpPawns = new TreeSet<MyPoint>(comp);
    for (int v = 0; v<4; v++) {
      if (genP(v)==1) {
        cel2.x=x; cel2.y=y+genDy(v); cel3.x = x+genDx(v); cel3.y = y;
      } else {
        cel2.x=x+genDx(v); cel2.y=y;cel3.x=x;cel3.y=y+genDy(v);
      }

      for (int xt = 1; xt<xs1; xt++)
        for (int yt = 1; yt<ys1; yt++)
          if (board[xt][yt]==div)
            tmpPawns.add(giveMyPoint(xt, yt));

      for (MyPoint pawn : tmpPawns) {
        for (int xd = -1; xd<=1; xd++)
          for (int yd = -1; yd<=1; yd++)
            if (pawns[pawn.x+xd][pawn.y+yd] < WALL)
              //nie byÅ‚o jeszcze nic - wstawiamy
              if (p == null) p = giveMyPoint(pawn.x+xd, pawn.y+yd);
              else {//byÅ‚ juÅ¼ punkt porÃ³wnujemy
                p2 = giveMyPoint(pawn.x+xd, pawn.y+yd);
                if (comp.compare(p, p2) > 0){//nowy najmnijeszy
                  p.x = p2.x; p.y = p2.y;
                }
                emptyMyPointList.addFirst(p2);
              }

        if (p!=null)
          emptyArray[p.x][p.y] += 1;

        p = null;
      }

      tmpAttackValue = countValue(pawns);

      //sprawdzamy czy nowa wersja ataku nie jest lepsza niÅ¼ stara..
      if (tmpAttackValue > ret.x) {
        ret.x = tmpAttackValue;
        ret.y = v;
      }

      //no i zerujemy wartoÅ›Ä‡ ataku..
      //tmpAttackValue = 0;

      while (!tmpPawns.isEmpty()){
        p = tmpPawns.first();
        tmpPawns.remove(p);
        emptyMyPointList.addFirst(p);
        p = null;//bo bez tego Å‚apie w pierwszym przebiegu  wartoÅ›Ä‡..:)
      }
    }

    return ret;
  }
  private static int mic = 0;

  //div zawsze jest w tym zastosowaniu znakiem zapytania.. '?'
  private static MyPoint micro(char div, char[][] board, int[][] pawns, TreeSet<MyPoint> checkList){
    int maxAttackValue = 0, v = 0;
    MyPoint best = giveMyPoint(), tmp, l;

    mic++;

    if (checkList == null)
      checkList = rndPoints(div, board, 4);

    while (!checkList.isEmpty()) {
      l = checkList.first();
      checkList.remove(l);
      if (l.x>xs1 || l.x<0 || l.y>ys1 || l.y<0)
        System.out.printf("l.x=%d, l.y=%d\n", l.x, l.y);

      tmp = myAttackValue(div, l.x, l.y, board, pawns);

      if (tmp.x>maxAttackValue) {
        best.x = l.x; best.y = l.y;
        maxAttackValue = tmp.x;
        v = tmp.y;
        emptyMyPointList.addFirst(tmp);

      } else if (tmp.x==maxAttackValue && rnd.nextBoolean()) {
        best.x = l.x; best.y = l.y;
        maxAttackValue = tmp.x;
        v = tmp.y;
        emptyMyPointList.addFirst(tmp);
      }
      emptyMyPointList.add(l);
    }

    dx = genDx(v);
    dy = genDy(v);
    p = genP(v);

    return best;
  }

  private static TreeSet<MyPoint> rndPoints (char div, char[][] iBoard, int time){

    Comparator<MyPoint> comp = new Comparator<MyPoint>(){
      public int compare(MyPoint p1, MyPoint p2) {
        return (p1.x-p2.x)+(p1.y-p2.y);
      }
    };

    TreeSet<MyPoint> checkList = new TreeSet<MyPoint>(comp);

    if (time > 1) {
      checkList.add(giveMyPoint(0,0));
      checkList.add(giveMyPoint(xs1,0));
      checkList.add(giveMyPoint(0,ys1));
      checkList.add(giveMyPoint(xs1,ys1));
    }

    for (int x = 1; x < xs1; x++)
      for (int y = 1; y < ys1; y++)
        if (iBoard[x][y]==div){
          if (time > 0 ) {
            checkList.add(giveMyPoint(0,y));
            checkList.add(giveMyPoint(xs1,y));
            checkList.add(giveMyPoint(x,0));
            checkList.add(giveMyPoint(x,ys1));

            checkList.add(giveMyPoint(x-1,y-1));
            checkList.add(giveMyPoint(x+1,y-1));
            checkList.add(giveMyPoint(x+1,y+1));
            checkList.add(giveMyPoint(x-1,y+1));
          }
          if (time > 1) {
            checkList.add(giveMyPoint(x-1,y));
            checkList.add(giveMyPoint(x+1,y));
            checkList.add(giveMyPoint(x,y+1));
            checkList.add(giveMyPoint(x,y-1));
          }
          if (time > 2) {
            checkList.add(giveMyPoint(x+(rnd.nextInt(xs-x)),y+(rnd.nextInt(ys-y))));
            checkList.add(giveMyPoint(x+(rnd.nextInt(xs-x)),y-(rnd.nextInt(y))));
            checkList.add(giveMyPoint(x-(rnd.nextInt(x)),y+(rnd.nextInt(ys-y))));
            checkList.add(giveMyPoint(x-(rnd.nextInt(x)),y-(rnd.nextInt(y))));
          }
        }

    if (time > 3) {
      checkList.add(giveMyPoint(rnd.nextInt(xs),rnd.nextInt(ys)));
      checkList.add(giveMyPoint(rnd.nextInt(xs),rnd.nextInt(ys)));
      checkList.add(giveMyPoint(rnd.nextInt(xs),rnd.nextInt(ys)));
      checkList.add(giveMyPoint(rnd.nextInt(xs),rnd.nextInt(ys)));
    }

    return checkList;
  }

  //zakÅ‚adam Å¼e na board sÄ… juÅ¼ naniesione znaki start i dst.
  private static MyPoint findWay(char start, char dst, char[][] flowBoard, int[][] flowTable, int[][] wallPenalty, int[][] pawnsPenalty){

    Comparator<FlowPoint> comp = new Comparator<FlowPoint>(){
      public int compare(FlowPoint p1, FlowPoint p2) {
        if (p1.x == p2.x && p1.y==p2.y) return 0;
        return p1.value-p2.value == 0 ? -1 : p1.value-p2.value;
      }

    };
    TreeSet<FlowPoint> visited = new TreeSet<FlowPoint>(comp);
    TreeSet<FlowPoint> nextPoint = new TreeSet<FlowPoint>(comp);
    FlowPoint foundPoint =giveFlowPoint(0, 0, Integer.MAX_VALUE);
    boolean found = false;
    //trzeba wyznaczyÄ‡ start pÅ‚ywu..
    for (int i = 1; i < xs1; i++)
      for (int j = 1; j < ys1; j++)
        if (flowBoard[i][j] == start)
          nextPoint.add(giveFlowPoint(i, j, 1));

    FlowPoint tp;
    int tx, ty, tv, i, j, tmpx = 0, tmpy = 0;
    char pointChar;

    while (!nextPoint.isEmpty()){//znajdywanie drogi do przeciwnika jak plansza otoczeni..:)
      tp = nextPoint.pollFirst();
      tx = tp.x; ty = tp.y; tv = tp.value;
      emptyFlowPointList.add(tp);

      for (i = -1; i <= 1; i++)
        for (j = -1; j <= 1; j++){
          if (flowTable[tx+i][ty+j] > 0) {//to pole juz bylo przetworzone przy jakiejs innej okazji

          } else if ((pointChar = flowBoard[tx+i][ty+j]) == start) {//nasz punkt startowy ok

            flowTable[tx+i][ty+j] = 1;
            tp = giveFlowPoint(tx+i, ty+j, 1);
            if (!visited.contains(tp)){
              nextPoint.add(tp);
              visited.add(tp);
            } else emptyFlowPointList.add(tp);

          } else if (pointChar == '#') {//sciana tego nie przetwarzamy:) nie mozna przez mniÄ… przejÅ›Ä‡

            flowTable[tx+i][ty+j] = 1048576;

          }  else if (pointChar == dst) {

            found = true;//juchu znaleÅºliÅ›my miejsce docelowe
            if (foundPoint.value>tv){//i jest blizej niz poprzednie!
              foundPoint.value=tv; foundPoint.x=tx; foundPoint.y=ty;
              /* System.out.printf("found value = %d\n", foundPoint.value);*/

            } else if (foundPoint.value==tv && rnd.nextBoolean()){//takie samo jak poprzednie moze sie nam podoba?
              foundPoint.value=tv; foundPoint.x=tx; foundPoint.y=ty;
              /*System.out.printf("found value = %d\n", foundPoint.value);*/
            }

          } else if (pawns[tx+i][ty+j] > 0) {

            tmpx = tv + 1/*PAWNSTANDINGPENALTY*/ /*+ pawns[tx+i][ty+j]*/ + wallPenalty[tx+i][ty+j] + pawnsPenalty[tx+i][ty+j];
            flowTable[tx+i][ty+j] = tmpx;
            nextPoint.add(giveFlowPoint(tx+i, ty+j, tmpx));

          } else { //pole nie przewidziane powyÅ¼ej np przeciwnik, czy trawa traktujemy spoko:)

            tmpx = tv + 1 + wallPenalty[tx+i][ty+j] + pawnsPenalty[tx+i][ty+j];
            flowTable[tx+i][ty+j] = tmpx;
            nextPoint.add(giveFlowPoint(tx+i, ty+j, tmpx));

          }
        }

      /*     for (int w = 0; w< xs; w++)
     System.out.printf("%s\n", Arrays.toString(flowTable[w]));*/

    }

    if (!found) return giveMyPoint(0, 0);//nie udaÅ‚o siÄ™ znaleÅºÄ‡:(

    found = false;
    tx = foundPoint.x;
    ty = foundPoint.y;
    //System.out.printf("found.x=%d found.y=%d\n", foundPoint.x, foundPoint.y);

    tv = 1048576;//znajdujemy od najwiekszego moÅ¼liwego punktu
    while (!found){//znajdywanie drogi od przeciwnika do pionka z dywizji

      for (i = -1; i <= 1; i++)
        for (j = -1; j <= 1; j++)
          switch(flowTable[tx+i][ty+j]){
            case 0 : break;//to pole nie bylo przetwarzane olewamy
            case 1 : /*flowBoard[tx][ty] = '+';*/ return giveMyPoint(tx, ty);
            default: if (tv>flowTable[tx+i][ty+j]) {
              tv = flowTable[tx+i][ty+j];
              tmpx = tx+i; tmpy = ty+j;
            }
          }

      /*flowBoard[tmpx][tmpy] = '*';*/

      if (!found) {
        tx = tmpx; ty = tmpy;
      }
    }

    //zaÅ›lepka gdyby wywoÅ‚ane byÅ‚o coÅ› nie tak..
    return giveMyPoint(xs1,ys1);
  }

  private static final Map<Character, MyPoint> myDivLose = new HashMap<Character, MyPoint>(32);

  //metoda wylicza czy nalezy sie wycofaÄ‡?
  private static boolean fallBack(char div) {
    Comparator<MyPoint> comp = new Comparator<MyPoint>(){
      public int compare(MyPoint p1, MyPoint p2) {
        return p1.x-p2.x+p1.y-p2.y;
      }
    };
    TreeSet<MyPoint> enemies = new TreeSet<MyPoint>(comp);

    int i, myValue = 0, enemyValue = 0;//iloÅ›Ä‡ przeciwnikÃ³w stykajÄ…cych siÄ™ z danym polem
    for (int x = 1; x < xs1; x++)
      for (int y = 1; y < ys1; y++)
        if (board[x][y]==div) {
          i = 1;
          if (pawns[x-1][y-1]<WALL) {i++; enemies.add(giveMyPoint(x-1,y-1));}
          if (pawns[x][y-1]<WALL) {i++; enemies.add(giveMyPoint(x,y-1));}
          if (pawns[x+1][y-1]<WALL) {i++; enemies.add(giveMyPoint(x+1,y-1));}
          if (pawns[x-1][y]<WALL) {i++; enemies.add(giveMyPoint(x-1,y));}
          if (pawns[x+1][y]<WALL) {i++; enemies.add(giveMyPoint(x+1,y));}
          if (pawns[x-1][y+1]<WALL) {i++; enemies.add(giveMyPoint(x-1,y+1));}
          if (pawns[x][y+1]<WALL) {i++; enemies.add(giveMyPoint(x,y+1));}
          if (pawns[x+1][y+1]<WALL) {i++; enemies.add(giveMyPoint(x+1,y+1));}

          if (i>1)
            myValue+= 1 + pawns[x][y]/i;
        }

    MyPoint e;
    while (!enemies.isEmpty()){
      i = 1;
      e = enemies.pollFirst();
      if (pawns[e.x-1][e.y-1]>GRASS) i++;
      if (pawns[e.x][e.y-1]>GRASS) i++;
      if (pawns[e.x+1][e.y-1]>GRASS) i++;
      if (pawns[e.x-1][e.y]>GRASS) i++;
      if (pawns[e.x+1][e.y]>GRASS) i++;
      if (pawns[e.x-1][e.y+1]>GRASS) i++;
      if (pawns[e.x][e.y+1]>GRASS) i++;
      if (pawns[e.x+1][e.y+1]>GRASS) i++;
      enemyValue+= 1 + ((-pawns[e.x][e.y])+WALL)/i;
    }

    if (enemyValue>retreatMultiplayer*myValue){//przegrywamy
      MyPoint losing;//jak bardzo przegrywamy na pierwszej zmiennej wynik przeciwnika na 2 nasz..
      if (!myDivLose.containsKey(div)) {//przegrywamy ale pierwszy raz wiÄ™c spoko jeszcze nic siÄ™ nie staÅ‚o..
        losing = giveMyPoint(enemyValue, myValue);
        myDivLose.put(div, losing);

        if (enemyValue>(retreatMultiplayer+retreatMultiplayer)*myValue)//przewaga przeciwnika jest duÅ¼a wiÄ™c jednak spadajmy
          return true;

        return false;//
      }

      losing = myDivLose.get(div);


      if (losing.y>=myValue)//nie przybyÅ‚y posiÅ‚ki.. wiÄ™c spierdzielajmy
        return true;

      if (enemyValue>(retreatMultiplayer+retreatMultiplayer)*myValue)//przewaga przeciwnika jest duÅ¼a wiÄ™c jednak spadajmy
        return true;

      if (losing.y<myValue){//przybyÅ‚y jakieÅ› posiÅ‚ki wiÄ™c dajmy jeszcze sznase tej pozycji..
        losing.y=myValue;
        return false;
      }


    } else {
      myDivLose.remove(div);
      return false;
    }
    //System.out.println("eV="+enemyValue+" > "+retreatMultiplayer+"*myV="+ myValue);
    return true;

    //uciekamy jeÅ›li przeciwnik ma ponad dwokrotna przewage
    //return enemyValue>retreatMultiplayer*myValue;
  }

  //cÃ³Å¼.. uciekamy!
  private static MyPoint retreat(char div){
    prepareRetreatFlowBoard(div);
    countEnemyTouchingPawns(100);//kara za to, Å¼e pole styka siÄ™ z przeciwnikiem
    zeroFlowTable();


    at = 0;//nie chcemy atakowaÄ‡ uciekajÄ…c..
    MyPoint p = findWay(div, '!', flowBoard, flowTable, emptyArray, touchingPawns);

    //skoro juÅ¼ doszliÅ›my tam gdzie mieliÅ›my spierdzielajÄ…c to pora walczyÄ‡! do ostatniej kropli krwi
    for (int i = -1; i <= 1; i++)
      for (int j = -1; j <= 1; j++)
        if (flowBoard[p.x+i][p.y+j] == '!' && board[p.x][p.y] == div){
          p.x = 100; return p;
        }

    if (board[p.x][p.y] == div)
    {p.x = 100; return p;}

    return p;
  }

  //przygotowujemy tablice pÅ‚ywyow..
  private static boolean fightOnBoard(){

    for (int x = 1; x < xs1; x++)
      for (int y = 1; y < ys1; y++)
        if (pawns[x][y] < WALL ) //to jest pionek przeciwnika czy styka siÄ™ z naszym?
          for (int i = -1; i <= 1; i++)
            for (int j = -1; j <= 1; j++)
              if (pawns[x+i][y+j] > GRASS)//nasz pionek
                //nasz pionek siÄ™ styka z przeciwnikiem wiÄ™c tu checmy iÅ›Ä‡ i mu pomÃ³c:)
                return true;

    return false;
  }

  //przygotowujemy tablice pÅ‚ywyow.. od razu
  private static void prepareReinforcementsFlowBoard(char div){
    for (int x = 1; x < xs1; x++)
      for (int y = 1; y < ys1; y++){

        if (pawns[x][y] < WALL ) {//to jest pionek przeciwnika czy styka siÄ™ z naszym?

          for (int i = -1; i <= 1; i++)
            for (int j = -1; j <= 1; j++)
              if (pawns[x+i][y+j] > GRASS && board[x+i][y+j] == div){//nasz pionek z naszej dywizji siÄ™ styka
                //jego juz nie ruszmy wiÄ™c '?'
                flowBoard[x][y] = '!';
                flowBoard[x+i][y+j] = '?';
              } else if (pawns[x+i][y+j] > GRASS){
                //tu jakiÅ› inny nasz pionek siÄ™ styka
                flowBoard[x][y] = '!';
              }

        } else  if (flowBoard[x][y] != '?'){
          flowBoard[x][y] = board[x][y];
        }
      }
  }

  //cÃ³Å¼.. pomagamy!!
  private static MyPoint reinforcements(char div){
    prepareReinforcementsFlowBoard(div);
    countTouchingPawns(PAWNSTANDINGPENALTY, reiTouchingPawnsPenalty, 0);//kara za to, Å¼e pole styka siÄ™ z naszymi
    zeroFlowTable();

    return findWay(div, '!', flowBoard, flowTable, touchingWalls, touchingPawns);

    //return p;
  }

  private static TreeSet<MyPoint> path(char div, MyPoint move, char[][] iBoard){
    Comparator<MyPoint> comp = new Comparator<MyPoint>(){
      public int compare(MyPoint p1, MyPoint p2) {
        return (p1.x-p2.x)+(p1.y-p2.y);
      }
    };

    TreeSet<MyPoint> checkList = new TreeSet<MyPoint>(comp);
    checkList.add(giveMyPoint(move.x, move.y));

    int tx = -2, ty = -2;

    for (int i = -1; i<2; i++)
      for (int j = -1; j<2; j++)
        if (iBoard[move.x+i][move.y+j] == div)
          if (tx == -2) {
            tx = i; ty = j;
          } else if (tx>-2){
            tx = -3;
          }

    //wiÄ™cej niÅ¼ jeden pionek siÄ™ styka.. nie wiem y w ktÃ³rÄ… stronÄ™ chcemy iÅ›Ä‡..
    if (tx < -1) {emptyMyPointList.add(move); return checkList;}

    //wiemy w ktÃ³ra strone iÅ›Ä‡ bo jeden pionek siÄ™ styka!
    fullpath++;

    while (move.x>0 && move.x<xs1 && move.y>0 && move.y<ys1){
      move.x-=tx;move.y-=ty;
      checkList.add(giveMyPoint(move.x,move.y));
      //System.out.printf("m.x=%d, m.y=%d\n",move.x,move.y);
    }

    emptyMyPointList.add(move);

    return checkList;
  }

  //przygotowujemy tablicÄ™ ustwienia taktycznego..:)
  private static void prepareTacticFlowBoard(){
    for (int x = 1; x < xs1; x++)
      for (int y = 1; y < ys1; y++)

        if (pawns[x][y] < WALL ) //to jest pionek przeciwnika CHCEMY WSZYSTKO WOKÃ“Å
          for (int i = -2; i <= 2; i+=4)
            for (int j = -2; j <= 2; j++)
              if (x+i<xs && x+i>0 && y+j<ys && y+j>0)
                if (board[x+i][y+j] != '#')
                  flowBoard[x+i][y+j] = '!';
  }

  private static int fullpath = 0;
  private static int fall = 0;
  private static int rei = 0;
  private static int tac = 0;

  private static MyPoint genStr(char div){
    MyPoint pt;

    //czy siÄ™ wycofywaÄ‡??
    if (fallBack(div)){
      pt = retreat(div);
      //moÅ¼na uciekaÄ‡:)
      if (pt.x != 100) {fall++;return pt;}
    }

    at = 1;//nie uciekamy albo juÅ¼ uciekliÅ›my w kozi rÃ³g. wiÄ™c walka!

    pt = prepareFlowBoard(div);//wynik w flowBoard moja dywizja to div wszyscy przecinicy to '!'

    if (pt.x == pt.y) //wsystkie pionki siÄ™ stykajÄ…..  p.x to iloÅ›Ä‡ pionkÃ³w p.y to iloÅ›Ä‡ tych ktÃ³re stykajÄ… siÄ™ z wrogiem
      return micro('?', flowBoard, pawns, null);//to robimy micro:)

    //nie wszyscy moi siÄ™ stykajÄ… no to co siÄ™ dzieje na planszy?

    //czy na planszy juÅ¼ walka siÄ™ toczy?
    if (fightOnBoard() && pt.y>0){//nasz oddziaÅ‚ sie bije
      pt = reinforcements(div);//trzeba pomÃ³c tym co walczÄ…!

      //if (pt.x == 0 && pt.y == 0) System.out.printf("CoÅ› nie tak1..\n");

      if (board[pt.x][pt.y] != '.'){
        //hm.. nie idziemy na trawÄ™ wiÄ™c pewnie siÄ™ gdzieÅ› siÄ™ stÅ‚oczyliÅ›my.. no to zrÃ³bmy micro:)
        microtac++;
        return micro(div, board, pawns, null);
      }

      //nie stÅ‚oczylismy siÄ™ no to zobaczmy czy jestesmy w stanie wyliczyÄ‡ gdzie idziemy i zrobiÄ‡ micro;)
      micromove++;
      return micro(div, board, pawns, path(div, pt, board));

      /*if (!(pt.x == 0 && pt.y == 0)) {rei++;return pt;}
      else System.out.printf("CoÅ› nie tak2..\n");*/
    } else {//nasz oddziaÅ‚ siÄ™ nie bije..
      pt = reinforcements(div);//trzeba pomÃ³c tym co walczÄ…!
      if (!(pt.x == 0 && pt.y == 0)) {rei++;return pt;}
      //else System.out.printf("CoÅ› nie tak3..\n");
    }


    //cÃ³Å¼ na planszy nikt siÄ™ nie bije  no to siÄ™ ostroÅ¼nie ustawiamy aby miec przewagÄ™ taktycznÄ…:).
    prepareTacticFlowBoard();
    countTouchingPawns(PAWNSTANDINGPENALTY, normalTouchingPawnsPenalty, WALLTOUCHINGPENALTY);//wyliczam karÄ™ za pionki ktÃ³re siÄ™ stykajÄ…
    zeroFlowTable();//zerujÄ™ tablicÄ™

    /* for (int i = 0; i<xs; i++)
   System.out.printf("%s\n", Arrays.toString(flowBoard[i]));*/

    pt = findWay(div, '!', flowBoard, flowTable, touchingWalls, touchingPawns);

    tac++;

    return pt;
  }

  private static int microtac = 0;
  private static int micromove = 0;

  private static TreeSet<MyPoint> microCheckPoints(char div){
    Comparator<MyPoint> comp = new Comparator<MyPoint>(){
      public int compare(MyPoint p1, MyPoint p2) {
        return (p1.x-p2.x)+(p1.y-p2.y);
      }
    };
    TreeSet<MyPoint> checkList = new TreeSet<MyPoint>(comp);

    for (int x = 1; x<xs1; x++)
      for (int y = 1; y<xs1; y++)
        if (board[x][y] == div )
          for (int i = -1; i <= 1; i++)
            for (int j = -1; j <= 1; j++)
              if (pawns[x+i][y+j] < WALL){
                checkList.add(giveMyPoint(x+i,y+j));
                checkList.add(giveMyPoint(x,y));
              }

    return checkList;
  }

  private static void genMove(char div){

    dx=rnd.nextInt(2)*2-1;
    dy=rnd.nextInt(2)*2-1;
    p=rnd.nextInt(2);
    at=1;

    MyPoint move;

    if (PLANSZA==SIEK ){

      if (!microCheckPoints(div).isEmpty() && myStr.containsKey(div) && !myStr.get(div).isEmpty())
        move = myStr.get(div).pollFirst();
      else{
        myStr.remove(div);
        move = reinforcements(div);
        if (move.x==0 && move.y==0) {move.x=1; move.y=1;}
        move = micro(div, board, pawns, path(div, move, board));
        rei++;
      }

    } else {
      if (myStr.containsKey(div) && !myStr.get(div).isEmpty()) {
        move = myStr.get(div).pollFirst();
        //gdy siÄ™ juÅ¼ stykam z wrogiem to startegia idzie do piachu..:)
        at = 0;
      } else
        move = genStr(div);
    }

    System.out.printf("= %d %d %d %d %d %d\n\n", at, move.y, move.x, dy, dx, p);
    emptyMyPointList.add(move);
  }

  boolean heuristicSet = false;

  private static final int WALLPENALTY = 1;

  private static final int WALLTOUCHINGPENALTY = 1;

  //staÅ‚a odpowiedzialna za ucieczke w przypadku przewagi przeciwnika
  //waga jest kwadratem staÅ‚ej 2 znaczy 2^2 czyli 4 razy przeciwnik musi byc mocniejszy abyÅ›my uciekali
  private static float retreatMultiplayer = 2;

  //stojacy pionek blokuje przejÅ›cie za tyle punktÃ³w
  private static int PAWNSTANDINGPENALTY = 10;

  //staÅ‚a odpowiedzialna za dzielenie wagi pionka w funkcji wyliczajÄ…ce odlegÅ‚oÅ›Ä‡ dla posiÅ‚kÃ³w
  private static int reiTouchingPawnsPenalty = 5;

  //staÅ‚a odpowiedzialna za dzielenie wagi pionka w funkcji wyliczajÄ…ce odlegÅ‚oÅ›Ä‡ dla
  //przypadku gdy nikt jeszcze nie atakuje..
  private static int normalTouchingPawnsPenalty = 5;

  private static void setItpw(){
    PLANSZA = ITPW;
    LinkedList<MyPoint> ll;
    int i;
    if (myDivSizeSmall){//seting a strategy
      ll = giveLinkedList();
      for (i=0; i<18; i++)
        ll.addLast(giveMyPoint(2, 25));
      for (i=0; i<6; i++)
        ll.addLast(giveMyPoint(10, 20));
      myStr.put('a', ll);

      ll = giveLinkedList();
      for (i=0; i<3; i++)
        ll.addLast(giveMyPoint(1, 1));
      ll.addLast(giveMyPoint(1, 4));
      for (i=0; i<19; i++)
        ll.addLast(giveMyPoint(1, 18));
      myStr.put('b', ll);

      ll = giveLinkedList();
      for (i=0; i<8; i++)
        ll.addLast(giveMyPoint(1, 1));
      for (i=0; i<19; i++)
        ll.addLast(giveMyPoint(4, 19));
      myStr.put('c', ll);

      ll = giveLinkedList();
      for (i=0; i<15; i++)
        ll.addLast(giveMyPoint(22, 19));
      for (i=0; i<8; i++)
        ll.addLast(giveMyPoint(14, 17));
      myStr.put('f', ll);

      ll = giveLinkedList();
      for (i=0; i<4; i++)
        ll.addLast(giveMyPoint(23, 1));
      for (i=0; i<18; i++)
        ll.addLast(giveMyPoint(22, 19));
      for (i=0; i<5; i++)
        ll.addLast(giveMyPoint(17, 18));
      myStr.put('e', ll);

      ll = giveLinkedList();
      for (i=0; i<9; i++)
        ll.addLast(giveMyPoint(23, 1));
      for (i=0; i<18; i++)
        ll.addLast(giveMyPoint(22, 19));
      myStr.put('d', ll);

      ll = giveLinkedList();
      for (i=0; i<5; i++)
        ll.addLast(giveMyPoint(13, 33));
      ll.addLast(giveMyPoint(20, 29));
      for (i=0; i<9; i++)
        ll.addLast(giveMyPoint(22, 25));
      for (i=0; i<5; i++)
        ll.addLast(giveMyPoint(21, 1));
      for (i=0; i<4; i++)
        ll.addLast(giveMyPoint(15, 15));
      myStr.put('g', ll);

    } else {
      ll = giveLinkedList();
      for (i=0; i<25; i++)
        ll.addLast(giveMyPoint(2, 33));
      myStr.put('A', ll);

      ll = giveLinkedList();
      for (i=0; i<25; i++)
        ll.addLast(giveMyPoint(5, 30));
      myStr.put('B', ll);

      ll = giveLinkedList();
      for (i=0; i<25; i++)
        ll.addLast(giveMyPoint(22, 28));
      myStr.put('F', ll);

      ll = giveLinkedList();
      for (i=0; i<25; i++)
        ll.addLast(giveMyPoint(17, 28));
      myStr.put('E', ll);
    }
  }

  private static void setLab(){
    PLANSZA=LAB;
    LinkedList<MyPoint> ll;
    int i;
    if (myDivSizeSmall){//seting a strategy
      ll = giveLinkedList();
      for (i=0; i<12; i++)
        ll.addLast(giveMyPoint(36, 48));
      myStr.put('d', ll);

      ll = giveLinkedList();
      ll.addLast(giveMyPoint(23, 3));
      for (i=0; i<12; i++)
        ll.addLast(giveMyPoint(0, 3));
      myStr.put('e', ll);
    } else {
      ll = giveLinkedList();
      for (i=0; i<12; i++)
        ll.addLast(giveMyPoint(12, 48));
      myStr.put('D', ll);

      ll = giveLinkedList();
      ll.addLast(giveMyPoint(23, 45));
      for (i=0; i<12; i++)
        ll.addLast(giveMyPoint(6, 45));
      myStr.put('E', ll);
    }
  }

  private static void setOtoczeni(){
    LinkedList<MyPoint> ll;
    PLANSZA=OTO;
    /* if (myDivSizeSmall){//seting a strategy
      ll = giveLinkedList();
      ll.addLast(giveMyPoint(12, 22));
      myStr.put('d', ll);

      ll = giveLinkedList();
      ll.addLast(giveMyPoint(12, 22));
      myStr.put('a', ll);

      ll = giveLinkedList();
      ll.addLast(giveMyPoint(12, 22));
      myStr.put('f', ll);

      ll = giveLinkedList();
      ll.addLast(giveMyPoint(12, 22));
      myStr.put('g', ll);

      ll = giveLinkedList();
      ll.addLast(giveMyPoint(12, 22));
      myStr.put('c', ll);
      ll = giveLinkedList();
      ll.addLast(giveMyPoint(12, 22));
      myStr.put('b', ll);
      ll = giveLinkedList();
      ll.addLast(giveMyPoint(12, 22));
      myStr.put('e', ll);
      ll = giveLinkedList();
      ll.addLast(giveMyPoint(12, 22));
      myStr.put('h', ll);
    } else {
      ll = giveLinkedList();
      ll.addLast(giveMyPoint(36, 22));
      myStr.put('M', ll);

      ll = giveLinkedList();
      ll.addLast(giveMyPoint(36, 22));
      myStr.put('J', ll);
     
      ll = giveLinkedList();
      ll.addLast(giveMyPoint(36, 22));
      myStr.put('0', ll);

      ll = giveLinkedList();
      ll.addLast(giveMyPoint(36, 22));
      myStr.put('P', ll);

      ll = giveLinkedList();
      ll.addLast(giveMyPoint(36, 22));
      myStr.put('L', ll);
      ll = giveLinkedList();
      ll.addLast(giveMyPoint(36, 22));
      myStr.put('K', ll);
      ll = giveLinkedList();
      ll.addLast(giveMyPoint(36, 22));
      myStr.put('N', ll);
      ll = giveLinkedList();
      ll.addLast(giveMyPoint(36, 22));
      myStr.put('Q', ll);
    }*/
  }

  private static void setSiekanka(){
    //System.out.printf("sk\n");
    PLANSZA=SIEK;
    PAWNSTANDINGPENALTY = 5;
    normalTouchingPawnsPenalty = 1;
    reiTouchingPawnsPenalty = 1;
    retreatMultiplayer = 5;

    LinkedList<MyPoint> ll;
    int i;
    char c;
    if (myDivSizeSmall){//seting a strategy

      for (c = 'a'; c<='x'; c++) {
        ll = giveLinkedList();

        for (i = 1; i< 60; i++){
          ll.addLast(giveMyPoint((c-'a')*2, 0));
          ll.addLast(giveMyPoint((c-'a')*2+2, 0));
        }


        myStr.put(c, ll);
      }
    } else {

      for (c = 'A'; c<='X'; c++) {
        ll = giveLinkedList();

        for (i = 1; i< 60; i++){
          ll.addLast(giveMyPoint((c-'A')*2, 49));
          ll.addLast(giveMyPoint((c-'A')*2+2, 49));
        }

        myStr.put(c, ll);
      }
    }

  }

  private static void setJeziora(){
    //System.out.printf("je\n");
    PLANSZA=JEZ;
    normalTouchingPawnsPenalty = 1;
    reiTouchingPawnsPenalty = 1;
    retreatMultiplayer = 1;
    PAWNSTANDINGPENALTY = 20;
  }

  private static void setZwykla(){
    PLANSZA = ZW;
    LinkedList<MyPoint> ll;
    int i;
    if (myDivSizeSmall){//seting a strategy
      ll = giveLinkedList();
      for (i=0; i<25; i++)
        ll.addLast(giveMyPoint(36, 1));
      myStr.put('a', ll);

      ll = giveLinkedList();
      for (i=0; i<26; i++)
        ll.addLast(giveMyPoint(28, 1));
      myStr.put('b', ll);

      ll = giveLinkedList();
      for (i=0; i<25; i++)
        ll.addLast(giveMyPoint(12, 38));
      myStr.put('e', ll);

      ll = giveLinkedList();
      for (i=0; i<25; i++)
        ll.addLast(giveMyPoint(13, 41));
      myStr.put('d', ll);

      if (rnd.nextBoolean()){
        ll = giveLinkedList();
        for (i=0; i<17; i++)
          ll.addLast(giveMyPoint(16, 1));
        myStr.put('c', ll);
      } else {
        ll = giveLinkedList();
        for (i=0; i<20; i++)
          ll.addLast(giveMyPoint(6, 31));
        myStr.put('c', ll);
      }

    } else {
      ll = giveLinkedList();
      for (i=0; i<26; i++)
        ll.addLast(giveMyPoint(45, 1));
      myStr.put('E', ll);

      ll = giveLinkedList();
      for (i=0; i<26; i++)
        ll.addLast(giveMyPoint(45, 17));
      myStr.put('D', ll);

      ll = giveLinkedList();
      for (i=0; i<26; i++)
        ll.addLast(giveMyPoint(22, 46));
      myStr.put('A', ll);

      ll = giveLinkedList();
      for (i=0; i<26; i++)
        ll.addLast(giveMyPoint(20, 37));
      myStr.put('B', ll);

      ll = giveLinkedList();
      for (i=0; i<26; i++)
        ll.addLast(giveMyPoint(27, 29));
      myStr.put('C', ll);
    }
  }

  private static void setLas(){
    PLANSZA=LAS;
    LinkedList<MyPoint> ll;
    int i;
    char c;
    if (myDivSizeSmall){//seting a strategy

      for (c = 'a'; c<'p'; c++) {
        ll = giveLinkedList();

        for (i = 1; i< 22; i++)
          ll.addLast(giveMyPoint(27, (c-'a')*3+4));

        myStr.put(c, ll);
      }
    } else {

      for (c = 'A'; c<'P'; c++) {
        ll = giveLinkedList();

        for (i = 1; i< 21; i++)
          ll.addLast(giveMyPoint(21,(c-'A')*3+4));

        myStr.put(c, ll);
      }
    }
  }

  private static void setPole(){
    PLANSZA=POLE;
    retreatMultiplayer = 2.5f;

    LinkedList<MyPoint> ll;
    int i;
    char c;
    if (myDivSizeSmall){//seting a strategy

      for (c = 'a'; c<'p'; c++) {
        ll = giveLinkedList();

        for (i = 1; i< 22; i++)
          ll.addLast(giveMyPoint(27, (c-'a')*3+4));

        myStr.put(c, ll);
      }
    } else {

      for (c = 'A'; c<'P'; c++) {
        ll = giveLinkedList();

        for (i = 1; i< 21; i++)
          ll.addLast(giveMyPoint(21,(c-'A')*3+4));

        myStr.put(c, ll);
      }
    }
  }

  private static final Map<Character, LinkedList<MyPoint>> myStr = new HashMap<Character, LinkedList<MyPoint>>(32);
  private static final Map<Character, Set<FlowPoint>> myDiv = new HashMap<Character, Set<FlowPoint>>(32);
  //private static final Map<Character, Integer> myDivStr = new HashMap<Character, Integer>(32);
  private static final Map<Character, Set<FlowPoint>> enDiv = new HashMap<Character, Set<FlowPoint>>(32);
/*  private static int myAlive = 0;
  private static int enAlive = 0;*/

  private static void setHeuristicStrategies(){
    int hSS = 6 ;

    if (xs == 25) {hSS=9; setItpw();}
    else if (ys == 49) {hSS=34; setLab();}
    else if (xs == 49) {hSS=18; setOtoczeni();}
    else if (lives == 5) {hSS=48; setSiekanka();}
    else if (rounds == 500) {hSS=50; setJeziora();}
    else if (rounds == 800) {hSS=18; setZwykla();}
    else if (board[3][1] == '#') {hSS=4;setLas();}
    else if (board[3][1] == '.') {hSS=4;setPole();}
    else {/*System.out.printf("unknown map\n");*/}

    char p;
    Set<FlowPoint> hs;
    for (int i = 1; i < xs1; i++)
      for (int j = 1; j < ys1; j++)
        if ((p = board[i][j]) != '.' && p != '#')
          if (is_small(p) == myDivSizeSmall) {
            if (myDiv.containsKey(p)) hs = myDiv.get(p);
            else {
              hs = new HashSet<FlowPoint>(hSS);
              myDiv.put(p, hs);
              //myDivStr.put(p,0);
            }
            hs.add(giveFlowPoint(i,j,lives));
            //myAlive++;
          } else {
            if (enDiv.containsKey(p)) hs = enDiv.get(p);
            else {
              hs = new HashSet<FlowPoint>(hSS);
              enDiv.put(p, hs);
            }
            hs.add(giveFlowPoint(i,j,lives));
            //enAlive++;
          }
  }

  public void run() {
    try {
      char div;
      int A, x, y, dx, dy, p;
      BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
      boolean notFinish = true;
      while (notFinish) {
        StringTokenizer st = new StringTokenizer(br.readLine());
        String cmd = st.nextToken();
        if (cmd.compareTo("set_game") == 0) {
          ys = Integer.parseInt(st.nextToken());
          xs = Integer.parseInt(st.nextToken());
          rounds = Integer.parseInt(st.nextToken());
          lives = Integer.parseInt(st.nextToken());
          xs1 = xs - 1; ys1 = ys - 1;
          emptyArray = new int[xs][ys];
          touchingWalls = giveEmpty();
          touchingPawns = giveEmpty();
          pawns = giveEmpty();
          flowTable = giveEmpty();
          board = new char[xs][ys];

          //System.out.printf("xs='%d', ys='%d'\n", xs, ys);
          for (int i = 0; i < xs; i++){//ys to wiersze..:(
            board[i] = st.nextToken().toCharArray();
            //System.out.printf("board[%d]='%s'\n", i, Arrays.toString(board[i]));
          }
          flowBoard = cloneBoard(board);
          countTouchingWalls();//przeliczam pierwszy i ost raz :) iloÅ›Ä‡ Å›cian
          ok();
        } else if (cmd.compareTo("time_left") == 0) {
          /*time_left = */Integer.parseInt(st.nextToken());
          //System.out.printf("time_left=%d\n", time_left);
          ok();
        } else if (cmd.compareTo("play")==0) {
          div = st.nextToken().charAt(0);
          A = Integer.parseInt(st.nextToken());
          //x = Integer.parseInt(st.nextToken());
          //y = Integer.parseInt(st.nextToken());
          y = Integer.parseInt(st.nextToken());
          x = Integer.parseInt(st.nextToken());
          dx = Integer.parseInt(st.nextToken());
          dy = Integer.parseInt(st.nextToken());
          p = Integer.parseInt(st.nextToken());
          //System.out.printf("play %c %d %d %d %d %d %d\n", div, A, x, y, dx, dy, p);
          if (pawns[0][0] != WALL){
            myDivSizeSmall = true;
            makePawns();
          }
          play(div, A, x, y, dy, dx, p, board, pawns);//tu x i y jest specjalnie zamienione i dx i dy
          ok();
        } else if (cmd.compareTo("gen_move") == 0) {
          div = st.nextToken().charAt(0);

          if (pawns[0][0] != WALL){
            myDivSizeSmall = false;
            makePawns();
          }

          if (!heuristicSet) {//ustawiam heurystyczne strategie..
            heuristicSet = true;
            setHeuristicStrategies();
          }

          genMove(div);

        } else if (cmd.compareTo("quit")==0) {

          for (int i = 0; i < xs; i++)
            System.out.printf("board[%d]='%s'\n", i, Arrays.toString(board[i]));
          for (int i = 0; i < xs; i++)
            System.out.printf("pawns[%d]='%s'\n", i, Arrays.toString(pawns[i]));

          System.out.printf("full micro executed='%d'\n", mic);
          System.out.printf("fall back executed='%d'\n", fall);
          System.out.printf("reinforcements executed='%d'\n", rei);
          System.out.printf("tactic executed='%d'\n", tac);
          System.out.printf("micro tactic executed='%d'\n", microtac);
          System.out.printf("micro move executed='%d'\n", micromove);
          System.out.printf("micro move fullPath executed='%d'\n", fullpath);

          ok();
          notFinish = false;
        } else if (cmd.compareTo("gen")==0) {
          div = st.nextToken().charAt(0);

          System.out.printf("gen='%c'\n", div);
        } else {
          error();
          notFinish = false;
        }

        System.out.flush();
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}