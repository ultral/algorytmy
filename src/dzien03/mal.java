import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class mal {

  private static int n;
  private static int kwa[][];
  private static int save[][];
  private static long suma = 0;
  private static long przeWsz = 0;
  private static int x1, y1, x2, y2;
  private static int xl1, yl1, xl2, yl2;


  private static void readInput() {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    try {
      StringTokenizer st = new StringTokenizer(br.readLine());
      n = Integer.parseInt(st.nextToken());
      kwa = new int[n][4];
      save = new int[n][4];
      for (int i = 0; i < n; i++) {
        st = new StringTokenizer(br.readLine());
        kwa[i][0] = Integer.parseInt(st.nextToken());
        kwa[i][1] = Integer.parseInt(st.nextToken());
        kwa[i][2] = Integer.parseInt(st.nextToken());
        kwa[i][3] = Integer.parseInt(st.nextToken());
      }
/*      for (int i = 0; i < n; i++) {
        System.out.printf("kwa[%d]='%s'\n", i, Arrays.toString(kwa[i]));
      }*/


    } catch (IOException e) {
      System.out.print("IOException: " + e.toString());
    }

  }

  private static void writeOutput() {
    System.out.print(suma);
  }

  private static boolean przetnijWszystkieBez(int x) {

    if (x == 0) {
      x1 = kwa[1][0];
      y1 = kwa[1][1];
      x2 = kwa[1][2];
      y2 = kwa[1][3];
    } else {
      x1 = kwa[0][0];
      y1 = kwa[0][1];
      x2 = kwa[0][2];
      y2 = kwa[0][3];
    }
    for (int i = 0; i < n; i++) {
      if (i != x) {
        x1 = Math.max(x1, kwa[i][0]);
        y1 = Math.max(y1, kwa[i][1]);
        x2 = Math.min(x2, kwa[i][2]);
        y2 = Math.min(y2, kwa[i][3]);
      }
      if (x1 > x2 && y1 > y2) {
        return false;
      }
    }

    return true;
  }

  private static void wypiszSave() {
    for (int i = 0; i < n; i++) {
      System.out.printf("save[%d]='%s'\n", i, Arrays.toString(save[i]));
    }
  }

  private static long doSave() {

    save[0][0] = 0;
    save[0][1] = 0;
    save[0][2] = Integer.MAX_VALUE;
    save[0][3] = Integer.MAX_VALUE;

    for (int i = 1; i < n; i++) {

      save[i][0] = Math.max(save[i-1][0], kwa[i-1][0]);
      save[i][1] = Math.max(save[i-1][1], kwa[i-1][1]);
      save[i][2] = Math.min(save[i-1][2], kwa[i-1][2]);
      save[i][3] = Math.min(save[i-1][3], kwa[i-1][3]);

    }

    //wypiszSave();

    x1 = kwa[n-1][0];
    y1 = kwa[n-1][1];
    x2 = kwa[n-1][2];
    y2 = kwa[n-1][3];

    for (int i = n-2; i > -1; i--) {

      save[i][0] = Math.max(x1, save[i][0]);
      save[i][1] = Math.max(y1, save[i][1]);
      save[i][2] = Math.min(x2, save[i][2]);
      save[i][3] = Math.min(y2, save[i][3]);

      x1 = Math.max(x1, kwa[i][0]);
      y1 = Math.max(y1, kwa[i][1]);
      x2 = Math.min(x2, kwa[i][2]);
      y2 = Math.min(y2, kwa[i][3]);
    }

    //wypiszSave();

    long sum = 0;

    for (int i = 0; i < n; i++){
      sum += policzPole(save[i][0], save[i][1], save[i][2], save[i][3]) - przeWsz;
    }

    return sum;
  }

  private static long policzPole(int x1, int y1, int x2, int y2) {
    return ((long) (x2 - x1)) * ((long) (y2 - y1));
  }

  private static long policzWszystkiePrzeciecia() {
    long sum = 0 ;
    for (int i = 0; i < n; i++) {
      if (przetnijWszystkieBez(i)) {
        sum += policzPole(x1, y1, x2, y2) - przeWsz;
      }
    }
    return sum;
  }

  private static void doMagic() {


    if (przetnijWszystkieBez(-1)) {
      przeWsz = policzPole(x1, y1, x2, y2);
    }

    suma += przeWsz;
    suma += doSave();
    //System.out.println("doSave(): " + (suma + doSave()));
    //System.out.println("policzWszystkiePrzeciecia(): " + (suma + policzWszystkiePrzeciecia()));

  }

  public static void main(String[] args) {
    readInput();
    doMagic();
    writeOutput();
  }
}
